'use strict';
var videoData;
var currentPosition = 0;
var videoDataToStore = {};
var id_next;
var next_bandid;
var next_showname;
var season_count,season_c;
var espisode_count;

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
var vid = getQueryStringValue("videoid");
//alert("vid+"+eid);
 id_next = getQueryStringValue("vid");
 espisode_count = getQueryStringValue("espisode");
 // season_count = getQueryStringValue("season");
  season_c = localStorage.getItem('season'); 
  season_count = parseInt(season_c);

   // alert("The season number is "+season_c);
   // alert("The inc season number is "+(parseInt(season_c) +1));
//alert("id_next"+id_next);

var videosArray = localStorage.getItem('videos') ? JSON.parse(localStorage.getItem('videos')) : [];
var bolt_account = JSON.parse(localStorage.getItem('bolt_account'));
var studio_account = JSON.parse(localStorage.getItem('studio_account'));
// var options = {
//   "type": vid,
//   "accountId": studio_account.account_id,
//   "policyKey": bolt_account.policy_key
// }

console.log(bolt_account);
console.log(studio_account);
$(document).ready(function () {
    localStorage.removeItem('detailsToPlayVideo');
    localStorage.removeItem('premium_redirect');

   var access_token = localStorage.getItem("access_token");
    
    $('#loading-overlay').show();
    $('#myPlayerID').attr('data-application-id', bolt_account.policy_key);
    var assetDetails = {
        "asset_ids": id_next,
        "detailsType": "all",
        "deviceDetails": {
            "deviceId": "71066286297",
            "mfg": "Google Chrome",
            "model": "Google Chrome",
            "os": "Android",
            "osVer": "XXX"
        },
        "isDetailedView": true
    }

    $.ajax({
        url: 'https://api.sonyliv.com/api/v4/vod/asset/details',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'x-via-device': 'true',
	    'Authorization': access_token,
        },
        method: 'POST',
        dataType: 'json',
        data: assetDetails,
        error: function (jqXHR, status, error) {
            configureAndPlayVideo(videoData);
            // if (jqXHR.status == 400) {
            //  alert("Sorry, Invalid/Expired OTP ");

            // }
            // if (jqXHR.status == 500) {
            // alert("Internal server error ");
            // }
            // else {
            //  alert("An error occurred: " + status + "nError: " + error);
            // }
            // $('#loading-overlay').hide();
        },
        success: function (data, status) {
            // $('#loading-overlay').hide();
            videoData = data.assets[0].details;
	    console.log(" Video Data in the asset details "+data);
	    console.log(" Json stringigy Video Data in the asset details "+JSON.stringify(data));
            next_showname = data.assets[0].details.showname;
	   if(data.assets[0].details.xdr != null)
	    currentPosition = data.assets[0].details.xdr.currentPosition/1000;
	   // alert("CUrrent position is "+currentPosition);
            configureAndPlayVideo(videoData);
        }
    });

});


function configureAndPlayVideo(videoDetail) {
    var myPlayer = bc('myPlayerID');
    var servUrl = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/423477888/SonyLiv/SonyLiv_Video_ConnectedTV&impl=s&cmsid=2461245&gdfp_req=1&ad_rule=1&env=vp&output=vmap&unviewed_position_start=1&url=[url]&referrer_url=[referrer_url]&description_url=http://www.sonyliv.com&vid=[vid]&correlator=1234";
    // var servUrl = "//pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/423477888/SonyLiv/SonyLiv_Video_ConnectedTV&impl=s&cmsid=2461245&gdfp_req=1&ad_rule=1&env=vp&output=vmap&unviewed_position_start=1&url=https://www.sonyliv.com/details/Episodes/5846044150001/Ep. 26 - The Thoughtful - Kaun Banega Crorepati Season 10 - 8 October 2018&description_url=http://www.sonyliv.com&vid=5846044150001&correlator=1234";
    if (videoDetail) {
        servUrl = servUrl.replace("[url]", "https://www.sonyliv.com/details/" + videoDetail.type + "/" + videoDetail.id + "/" + videoDetail.title);
        servUrl = servUrl.replace("[referrer_url]", "https://www.sonyliv.com/details/" + videoDetail.type + "/" + videoDetail.id + "/" + videoDetail.title);
        servUrl = servUrl.replace("[vid]", vid);
    }
    // console.log(servUrl);

    var active_Subscriptions = localStorage.getItem("activeSubscriptions_date");

   if(active_Subscriptions === "undefined" || active_Subscriptions === "" || active_Subscriptions === null)
   {
	    console.log("Ads Configration for the non premium user");
	    myPlayer.ima3({
		serverUrl: function (callback) {
		    callback(servUrl);
		    // callback('https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpremidpost&cmsid=496&vid=5845622238001&correlator=');
		    // callback('https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpremidpost&cmsid=496&vid=short_onecue&correlator=');
		},
		// timeout: 8000,
		requestMode: 'onload',
		// ad_type: 'skippablevideo',
		debug: true,
		debugContribAds: true
	    });

    }

    myPlayer.ready(function () {
        myPlayer = this;
        myPlayer.catalog.getVideo(vid, function (error, video) {
            try {
                myPlayer.catalog.load(video);
                $('#loading-overlay').hide();
                $('#playerContainer').show();
                
            } catch (error) {
                myPlayer.catalog.load(video);
                $('#loading-overlay').hide();
                $('#playerContainer').show();
            }
        });

	var videoStart = 0;
	var currentPositionTemp=0;
	videoStart = currentPosition;
        for (var i = 0; i < videosArray.length; i++) {
            if (videosArray[i].videoId == vid) {
                videoStart = Math.round(videosArray[i].videoStart);
            }
        }

       
//console.log("video start ==== "+videoStart);
//videoStart=12345;
	 // Wait till player metadata has loaded
   myPlayer.on("loadedmetadata", function () {
      var lang="en";
      // Open the English version captions
      for (var i = 0; i < (myPlayer.textTracks().length); i++) {
        if (myPlayer.textTracks()[i].language == lang) {
          // console.log(lang);
          // console.log(myPlayer.textTracks()[i].language);
          myPlayer.textTracks()[i].mode = "showing";
        }
      }
    });

        myPlayer.on("timeupdate", function (cTime) {
            //alert(1);
            //
            currentPosition = myPlayer.currentTime();
	    if(currentPositionTemp==0){
		currentPositionTemp++;
	    $(".vjs-menu-content .vjs-menu-item").toggleClass('vjs-selected');
		}

             if (Math.round(currentPosition) != videoStart) {
                 videoStart = Math.round(currentPosition);
             }
        });
        myPlayer.on("ended", function () {
            videoStart = 0;
       
	console.log("===Number of videos "+videosArray.length);
            if(parseInt(espisode_count)){
	console.log("===Episode count "+espisode_count);
                nextvideo(espisode_count);
    
            }
            else{
               // recomanded();

                id_next = getQueryStringValue("vid");
                // alert("id_next"+id_next);
  
                espisode_count = getQueryStringValue("espisode");
                  // alert("espisode_count"+espisode_count);
               // if(id_next=="")
                // {  //sathish
                        window.location.href=("recommended.html?videoid=" + id_next+ "&vid=" +id_next +"&espisode=" +espisode_count);
                  //  }
		  /*
                    else{
                    var access_token = localStorage.getItem("access_token");
                      
                             var showsearchXdrData = [{
                                    "action": "",
                                    "data": "xdr",
                                    "id": "xdr_rail",
                                    "pageNumber": "0",
                                    "pageSize": "10",
                                    "sortOrder": "",
                                    "type": "xdr"
                                }];          
                    var showQueryXdrData = {
                                    "detailsType": "basic",
                                    "deviceDetails": {
                                        "deviceId": "71066286297",
                                        "mfg": "GoogleChrome",
                                        "model": "GoogleChrome",
                                        "os": "Android",
                                        "osVer": "XXX"
                                    },
                                    "isSearchable": true,
                                    "searchSet": showsearchXdrData
                                };

                    $.ajax({
                            url: 'https://api.sonyliv.com/api/v4/vod/search',
                            // dataType: 'json',
                            // contentType: 'application/json',
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded',
                                'x-via-device': 'true',
                                'Authorization': access_token,
                                // 'Access-Control-Allow-Origin': 'https://www.sonyliv.com/',
                                // "Access-Control-Allow-Origin" : "*",
                            },
                            // data: showData,
                            // cache: false,
                            method: 'POST',
                            dataType: 'json',
                            data: showQueryXdrData,
                            error: function (jqXHR, status, error) {
                            //configureAndPlayVideo(videoData);
                                 if (jqXHR.status == 400) {
                                     alert(" Continue watching error 400 ");
                                    }
                                if (jqXHR.status == 500) {
                                 alert("Continue watching error 500 ");
                                    }
                            },
                            success: function (data, status) {
                                var current_video_id=0;
                                for(var i=0;i<data[0].assets.length;i++){
                                    
                                    if(current_video_id == 1){
                                        window.location.href=("player.html?videoid=" +data[0].assets[i].id+ "&vid=" +data[0].assets[i].vid +"&espisode=" +espisode_count);
                                        break;
                                    }
                                    console.log(data[0].assets[i].id);
                                    console.log(videoData.id);
                                    if(data[0].assets[i].id == videoData.id){
                                        current_video_id=1
                                    }
                                }
                                //console.log('success data :',data);
                                // alert('Success got data');
                                //window.location.href=("player.html?videoid=" + id_next+ "&vid=" +id_next +"&espisode=" +espisode_count);
                            }
                        });
                 } // sathish
                  */
               // window.location.href="recommended.html";
                  //window.location.href=("recommended.html?videoid=" + id_next+ "&vid=" +id_next +"&espisode=" +espisode_count);
          
                // $('body').css('background','#000');
                // $('#afterVideo').css('display','block');
            }
        });
        myPlayer.on("ads-load", function (someData) {
            console.log(myPlayer);
            var cuePointsAra = [],
                adCuePointsAra = [],
                videoDuration;
            // Extract cue points array from mediainfo
            cuePointsAra = myPlayer.mediainfo.cuePoints;
            // Extract ad cue points from all cue points
            adCuePointsAra = cuePointsAra.filter(isAdCuePoint);
            // Get the video duration
            videoDuration = myPlayer.mediainfo.duration;
            console.log(myPlayer.ima3);
           // var adsData = myPlayer.ima3.adsManager.Ca;
            //var adsData = myPlayer.ima3.adsManager.Fa;
           var adsData = myPlayer.ima3.adsManager.getCuePoints();
         console.log(adsData);
            for (var key in adsData) {
                adCuePointsAra.push({
                    "name": "Ad Cue Point",
                    "type": "AD",
                    "time": adsData[key],
                    "metadata": null,
                    "force_stop": false
                });
            }
            // Add cuePoint markers for ad cue points
            addAdMarkers(adCuePointsAra, videoDuration);
            if (videoStart > 0) {
                myPlayer.currentTime(parseInt(videoStart));
            }
            myPlayer.play();
          
        });

	if (videoStart > 0) {
                myPlayer.currentTime(parseInt(videoStart));
            }
            myPlayer.play();

        // Not Required because TV already supports this things

        $(window).keyup(function (e) {

	// showToast("Key Number is "+e.keyCode);
            $(".vjs-menu-item").attr('tabindex',0);
            if (e.keyCode == 37) { //remote left
		 // myPlayer.pause();
                // myPlayer.currentTime(parseInt(currentPosition - 30));
		// myPlayer.play();
		 myPlayer.focus('ControlBar');
            } else if (e.keyCode == 412) { //remote reverse
                 myPlayer.pause();
                myPlayer.currentTime(parseInt(currentPosition - 60));
                myPlayer.play();
            } else if (e.keyCode == 39) { // remote right
		 // myPlayer.pause();
                 // myPlayer.currentTime(parseInt(currentPosition + 60));
		 // myPlayer.play();
		 myPlayer.focus('ControlBar');
            } else if (e.keyCode == 417) { //remote forward
                // myPlayer.focus('ControlBar');
		 myPlayer.pause();
                  myPlayer.currentTime(parseInt(currentPosition + 60));
		  myPlayer.play();
            } else if (e.keyCode == 32 || e.keyCode == 13) { // Space
                if (myPlayer.paused()) {
                    myPlayer.play();
                } else  {
                    // myPlayer.pause();
                }
            } else if (e.keyCode == 19) { //Remote pause

		myPlayer.pause();
	    
            } else if (e.keyCode == 415) { //Remote play

		myPlayer.play();
		
            } else if (e.keyCode == 38) { //top
                // if (myPlayer.paused()) {
                  //   myPlayer.play();
		// }


            } else if (e.keyCode == 40) { //bottom
		
                $('.vjs-progress-control').focus();
                myPlayer.focus('ControlBar');
		//  myPlayer.pause();
                myPlayer.focus('FullscreenToggle');
            }
        });

    });
}


function showToast(msg) {
    var ele = document.getElementById("toast");
    if (ele !== null) {
        ele.innerHTML = msg;
        ele.className = "show";
        setTimeout(function () {
            var element = document.getElementById("toast");
            element.remove();
        }, 3000);
    } else {
        var newElem = document.createElement("div");
        document.body.appendChild(newElem);
        newElem.setAttribute("id", "toast");
        newElem.innerHTML = msg;
        newElem.className = "show";
        setTimeout(function () {
            var element = document.getElementById("toast");
            element.remove();  
        }, 3000);
    }
}   

// +++ Filter array on ad cue points +++
function isAdCuePoint(cuePoint) {
return (cuePoint.type == "AD");
}

// +++ Add AD cue point markers +++
function addAdMarkers(adCuePointsAra, videoDuration) {
    var iMax = adCuePointsAra.length,
        i,
        playheadWell = document.getElementsByClassName(
            "vjs-progress-control vjs-control"
        )[0];
    // Loop over each cue point, dynamically create a div for each
    // then place in div progress bar
    for (i = 0; i < iMax; i++) {
        var elem = document.createElement("div");
        elem.className = "vjs-marker";
        elem.id = "ad" + i;
        elem.style.left = adCuePointsAra[i].time / videoDuration * 100 + "%";
        playheadWell.appendChild(elem);
    }
}


// // +++ Filter array on ad cue points +++
// function isAdCuePoint(cuePoint) {
//     return cuePoint.type == "AD";
// }

// // +++ Add AD cue point markers +++
// function addAdMarkers(adCuePointsAra, videoDuration) {
//     var iMax = adCuePointsAra.length,
//         i,
//         playheadWell = document.getElementsByClassName(
//             "vjs-progress-control vjs-control"
//         )[0];
//     // Loop over each cue point, dynamically create a div for each
//     // then place in div progress bar
//     for (i = 0; i < iMax; i++) {
//         var elem = document.createElement("div");
//         elem.className = "vjs-marker";
//         elem.id = "ad" + i;
//         elem.style.left = adCuePointsAra[i].time / videoDuration * 100 + "%";
//         playheadWell.appendChild(elem);
//     }
// }

//if (videoData) {
    window.addEventListener('beforeunload', function (e) {
        // Cancel the event as stated by the standard.
        e.preventDefault();
        // Chrome requires returnValue to be set.
        e.returnValue = '';
        var total = videosArray.length;
 	// alert("Before sending to currentPosition "+currentPosition); 
 	console.log("Before sending to currentPosition "+currentPosition); 
	 // var request_json= new Array();
	 //

     if(currentPosition>60){

	var currentpos = Math.round(currentPosition*1000);

	// currentpos = Math.ceil(currentpos);
	// alert(" Current Position is "+currentpos+ "And total duration is "+videoData.duration);

	// alert("Video after 60 secs");
            // var request_json=[{"assetGenre":"","assetId":videoData.id,"assetShow":"","assetTitle":videoData.title,"offset":{"assetDuration":videoData.duration,"position":currentPosition},"updated_at":+ new Date()}]
	   
//		   var request_json ={"assetId":videoData.id,"assetTitle":videoData.title,"offset":{"assetDuration":videoData.duration,"position":currentpos},"updated_at":+ new Date()} ;
             var request_json=[{"assetId":parseInt(videoData.id),"assetGenre":"testContinueWatching","assetShow":"CWShow","assetTitle":videoData.title,"deviceId":"","offset":{"assetDuration":(videoData.duration/1000),"position":currentpos},"updated_at":+ new Date()}]; 

	// request_json.push(video_content);	
	 var currentPosPrecent=(currentPosition/( videoData.duration/1000))*100;

		 alert("Videos id is "+videoData.id+ "And Video vid is "+videoData.vid);
		 console.log("Videos id is "+videoData.id+ "And Video vid is "+videoData.vid);
	//    alert("The current pos percent is after playing 90% of video  "+currentPosPrecent);
	    console.log("The current pos percent is after playing 90% of video  "+currentPosPrecent);
	var access_token = localStorage.getItem("access_token");
	// alert("Before checking AT ");
	 if (access_token)   // Registered user
         {

            if(parseInt(currentPosPrecent)>90)
            {
		
		console.log("In the if scenraio ");
                deleteContinuesWatchingData(videoData.id);
		   var updatedVideosArray = [];
                for(var i = 0; i< videosArray.length; i++)
                {
                      //  console.log("VIdeos are "+videosArray[i].vid);
                       // alert("VIdeos are "+videosArray[i].vid);

                        // alert("VIdeos id "+vid);
                        if(videosArray[i].vid ===  vid)
                        {

                        // alert("= VIdeos are "+videosArray[i].vid);
                        // alert("= VIdeos id "+vid);


                         // videosArray = videosArray.splice(i);
                         // break;
                        }
                        else updatedVideosArray.push(videosArray[i]);

                }
                // videosArray.splice('vid:'+vid);
                localStorage.setItem('videos', JSON.stringify(updatedVideosArray));
            }
	    else
            {
		console.log("In the else scenraio ");
                 // sendContinuesWatchingData(request_json); 
		 if (total) {
                   videosArray = videosArray.filter(video => video.videoId != videoData.id);
               }
                // alert("Videos array length is "+videosArray.length);
                    videosArray.push({
                        tvBackgroundImage: videoData.tvBackgroundImage,
                        thumbnailUrl: videoData.thumbnailUrl,
                        subscriptionMode: videoData.subscriptionMode,
                        title: videoData.title,
                        shortDesc: videoData.shortDesc,
                        id: videoData.id,
                        vid: videoData.vid,
                        classification: videoData.classification,
                        type: videoData.type,
                        tvBackgroundImage: videoData.tvBackgroundImage,
                        videoId: vid,
                        videoStart: currentPosition,
                        totalLength: videoData.duration/1000
                    });

              /*
             */
            localStorage.setItem('videos', JSON.stringify(videosArray));
                 // sendXdrData(request_json); 

            }
                



	} 
	else // Guest User
	{


            if(parseInt(currentPosPrecent) < 90)
	    {
		// alert("In the else when no access token "+total);
		// alert("Videos array length is "+videosArray.length);

	       if (total) {
		   videosArray = videosArray.filter(video => video.videoId != videoData.id);
	       }
		 alert("Videos id is "+videoData.id+ "And Video vid is "+videoData.vid);
		    videosArray.push({
			tvBackgroundImage: videoData.tvBackgroundImage,
			thumbnailUrl: videoData.thumbnailUrl,
			subscriptionMode: videoData.subscriptionMode,
			title: videoData.title,
			shortDesc: videoData.shortDesc,
			id: videoData.id,
			vid: videoData.vid,
			classification: videoData.classification,
			type: videoData.type,
			tvBackgroundImage: videoData.tvBackgroundImage,
			videoId: vid,
			videoStart: currentPosition,
			totalLength: videoData.duration/1000
		    });

	      /*
             */
            localStorage.setItem('videos', JSON.stringify(videosArray));


           }
	   else 
           {
		
		// console.log("In the VIdeo array splice "+videosArray.indexOf('vid:'+vid));
		// alert("In the VIdeo array splice "+videosArray.indexOf('vid:'+vid));
		// videosArray.splice(videosArray.indexOf('vid:'+vid));
		var updatedVideosArray = [];
		for(var i = 0; i< videosArray.length; i++)
                {
                      //  console.log("VIdeos are "+videosArray[i].vid);
                       // alert("VIdeos are "+videosArray[i].vid);

                        // alert("VIdeos id "+vid);
                        if(videosArray[i].vid ===  vid)
                        {

                        // alert("= VIdeos are "+videosArray[i].vid);
                        // alert("= VIdeos id "+vid);


                         // videosArray = videosArray.splice(i);
                         // break;
                        }
			else updatedVideosArray.push(videosArray[i]);

                }
		// videosArray.splice('vid:'+vid);
            	localStorage.setItem('videos', JSON.stringify(updatedVideosArray));
		// alert("1 In the VIdeo array splice ");


            }  // ENd of checking the current position greater then 90% of video watching 

		var toRedirect = localStorage.getItem('detail_redirect');

		if(toRedirect !== null){
		    window.location.href = toRedirect;
		}
       } // End of guest user

      } // Checking current position > 60

    }); // ENd of event listener

//}

function nextvideo(espisode_count){
 

if(!localStorage.getItem("totalSeasons"))
season_count = null;
var eps_data = {
   "deviceDetails": {
        "deviceId": "2038fc47a16f0087",
        "mfg": "Google Chrome",
        "model": "Google Chrome",
        "os": "Android FireTv",
        "osVer": "XXX"
    },
    "episodeNumber": espisode_count,
    "season": season_count,
    "showName": next_showname
}
//console.log("eps_data"+ JSON.stringify(eps_data));
   $.ajax({
        url: 'https://api.sonyliv.com/api/v4/vod/videos/nextandpreviousEpisode',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'x-via-device': 'true',
            // 'Access-Control-Allow-Origin': 'https://www.sonyliv.com/',
            // "Access-Control-Allow-Origin" : "*",


        },
        method: 'POST',
        dataType: 'json',
        data: eps_data,
        error: function (jqXHR, status, error) {

        },
        success: function (data, status) {

            
	if(!localStorage.getItem("totalSeasons"))  // Shows 
        {	
             console.log("==="+ JSON.stringify(data.next));
            // console.log("===season"+ JSON.stringify(data.previous.season));

            if(data.next != undefined)	
	    {
		    var next_eposide = data.next.episode;
		    
		    var next_showname = data.next.showname;
		    var next_id = data.next.id;
		    var next_vid = data.next.vid;
		   // window.location.replace("player.html?videoid=" + vid + "&vid=" +vid_eps);
		    var next_id = data.next.id;
			
             	    window.location.href=("player.html?videoid=" + next_vid+ "&vid=" +next_id +"&espisode=" +next_eposide+"&none=type:show&none=type:promos");
	    }
	    else 
	    {
		console.log("Before calling recommended ");
		  id_next = getQueryStringValue("vid");
                // alert("id_next"+id_next);

                  espisode_count = getQueryStringValue("espisode");
                  // alert("espisode_count"+espisode_count);

               // window.location.href="recommended.html";
                  window.location.href=("recommended.html?videoid=" + id_next+ "&vid=" +id_next +"&espisode=" +espisode_count);
	    }

	  }
          else if(data.next != undefined) // Seasons
	   {

		  // alert("Season count when no episodes"+season_count)
		console.log("===IN the next episocde if ");
	        var next_eposide = data.next.episode;
	    
		var next_showname = data.next.showname;
		var next_id = data.next.id;
		var next_vid = data.next.vid;
		// season_count = null;
		season_c = localStorage.getItem("season");
		// season_count = parseInt(season_c);
		season_count = data.next.season;
		localStorage.setItem("season",season_count);
		console.log("Season count from localStorage"+localStorage.getItem("season"));
		console.log("Season count "+season_count);
			
			// season_count = parseInt(season_c);
		    // season_count = data.previous.season;
		   // window.location.replace("player.html?videoid=" + vid + "&vid=" +vid_eps);
		 window.location.href=("player.html?videoid=" + next_vid+ "&vid=" +next_id +"&season=" +season_count +"&espisode=" +next_eposide+"&none=type:show&none=type:promos");

           }
           else 
	  {
		console.log("===IN the next episocde else ");
	    var next_eposide = 0;
           //  var next_showname = data.next.showname;
            // var next_id = data.next.id;
            // var next_vid = data.next.vid;
               
		var totalSeasonCount = parseInt(localStorage.getItem("totalSeasons"));
	 	season_count = season_count + 1;
		season_count = data.next.season;
		console.log("Season Count else if "+data.next.season);

		// alert("Season count"+season_count);
		// alert("Total Season count"+totalSeasonCount);
		if(season_count <= totalSeasonCount)
                {

			localStorage.setItem("season",season_count);
		console.log("Season count from localStorage during season swicth"+localStorage.getItem("season"));
                	nextvideo(next_eposide);
            
		}
		else  
		{
                  window.location.href=("recommended.html?videoid=" + id_next+ "&vid=" +id_next +"&espisode=" +espisode_count);
	
		}
	
		
             //  recomanded();
               //  id_next = getQueryStrnext_eposide ingValue("vid");
               //  alert("id_next"+id_next);
  
               //    espisode_count = getQueryStringValue("espisode");
               //    alert("espisode_count"+espisode_count);
  
               // // window.location.href="recommended.html";
               //    window.location.href=("recommended.html?videoid=" + id_next+ "&vid=" +id_next +"&espisode=" +espisode_count);

	   }
      
                    // window.location.href="login.html";
            


            
        }
    });
}

function recomanded(){
  
  

    id_next = getQueryStringValue("vid");
  
 espisode_count = getQueryStringValue("espisode");

    console.log("Values at recommend id_next "+id_next+" Episode count "+espisode_count);


 // var search = rails=recommendation&item_id=5846247943001&page_id=music;
   var recomandedSearch = {
        "detailsType": "basic",
        "deviceDetails": {
            "deviceId": "71066286297",
            "mfg": "Google Chrome",
            "model": "Google Chrome",
            "os": "Android",
            "osVer": "XXX"
        },
        "isSearchable": true,
        "searchSet": [{
            "data": "rails=recommendation&item_id="+id_next + "&page_id="+espisode_count,
            "id": "reco_asset_based_default",
            "pageNumber": "0",
            "pageSize": 10,
            "sortOrder": "DISPLAY_NAME:ASC",
            "type": "recosense"
        }]
    }
    $.ajax({
        url: 'https://api.sonyliv.com/api/v4/vod/search',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'x-via-device': 'true',
            // 'Access-Control-Allow-Origin': 'https://www.sonyliv.com/',
            // "Access-Control-Allow-Origin" : "*",


        },
        method: 'POST',
        dataType: 'json',
        data: recomandedSearch,
        error: function (jqXHR, status, error) {

        },
        success: function (data, status) {

            console.log("==hhhh="+ JSON.stringify(data));

            var active_Subscriptions = localStorage.getItem("activeSubscriptions_date");
         

            if(!active_Subscriptions){

            for (var p = 0; p < data.length; p++){
                console.log("ooooo---"+ JSON.stringify(data[p].railName)); 
                console.log("railName" + data[p].railName);
                $("#videoEnded").append('<h3> ' + data[p].railName + '</h3>');
          
                var ulBody1 =  "<div><ul>";

                for (var recm_i = 0; recm_i < data[p].assets.length; recm_i++) {
                    data[p].assets[recm_i].thumbnailUrl = "https://resources.sonyliv.com/image/fetch/h_147,w_260,c_fill,fl_lossy,f_auto,q_auto,e_contrast:30,e_brightness:10/" + data[p].assets[recm_i].thumbnailUrl;
                    if (data[p].assets[recm_i].subscriptionMode == 'Free') {
                        ulBody1 += '<li><a  href="javascript:void(0);" onclick="movieDeatilsRedirect(\'' + data[p].assets[recm_i].id + '\');" ><img src= ' + data[p].assets[recm_i].thumbnailUrl + ' /></a></li>';
                    } else if (data[p].assets[recm_i].subscriptionMode == "SVOD") {
                        ulBody1 += '<li> <span><img src="../img/icon/ic_premium_unlocked.png"></span><a onclick="movieDeatilsRedirect(\'' + data[p].assets[recm_i].id + '\');"> <img src= ' + data[p].assets[recm_i].thumbnailUrl + ' /></a></li>';
                    } else if (data[p].assets[recm_i].subscriptionMode == "TVOD") {
                        ulBody1 += '<li><span ><img src="../img/icon/ic_rented.png"><a  onclick="movieDeatilsRedirect(\'' + data[p].assets[recm_i].id + '\');"> <img src= ' + data[p].assets[recm_i].thumbnailUrl + ' /></a></li>';

                    }

                }
                ulBody1 += '</ul> </div>';
             $("#videoEnded").append(ulBody1);
            }
          }
         else{
            for (var p = 0; p < data.length; p++){
                console.log("ooooo---"+ JSON.stringify(data[p].railName)); 
                console.log("railName" + data[p].railName);
                $("#videoEnded").append('<h3> ' + data[p].railName + '</h3>');
                var ulBody1 = "<div><ul>";

                for (var recm_i = 0; recm_i < data[p].assets.length; recm_i++) {
                    data[p].assets[recm_i].thumbnailUrl = "https://resources.sonyliv.com/image/fetch/h_147,w_260,c_fill,fl_lossy,f_auto,q_auto,e_contrast:30,e_brightness:10/" + data[p].assets[recm_i].thumbnailUrl;
                    if (data[p].assets[recm_i].subscriptionMode == 'Free') {
                        ulBody1 += '<li><a  href="javascript:void(0);" onclick="movieDeatilsRedirect(\'' + data[p].assets[recm_i].id + '\');" ><img src= ' + data[p].assets[recm_i].thumbnailUrl + ' /></a></li>';
                    } else if (data[p].assets[recm_i].subscriptionMode == "SVOD") {
                        ulBody1 += '<li> <span><img src="../img/icon/premium1.png"></span><a onclick="movieDeatilsRedirect(\'' + data[p].assets[recm_i].id + '\');"> <img src= ' + data[p].assets[recm_i].thumbnailUrl + ' /></a></li>';
                    } else if (data[p].assets[recm_i].subscriptionMode == "TVOD") {
                        ulBody1 += '<li><span ><img src="../img/icon/rent.png"><a  onclick="movieDeatilsRedirect(\'' + data[p].assets[recm_i].id + '\');"> <img src= ' + data[p].assets[recm_i].thumbnailUrl + ' /></a></li>';

                    }

                }
                ulBody1 += '</ul> </div>';
             $("#videoEnded").append(ulBody1);
            }


        }
            
           // alert(JSON.stringify(data[0].assets[0].thumbnailUrl));
           // console.log("ooooo---"+ JSON.stringify(data.assets[0].thumbnailUrl));
            //$("#videoEnded").append('<div class="col-md-2 slide"><button class="btn btn-episode" id ="episodecount"> E1-  ' + data.assets[i].details.episodeCount + '</button></div>');
           // $("#videoEnded").append('<li><a href="#"><img src=' + data[0].assets[0].thumbnailUrl + ' alt=""/></a></li>');
        
        }
    });
    
}

function sendContinuesWatchingData(request_json){
    var access_token = localStorage.getItem("access_token");
    if (access_token) {
     
        $.ajax({
            url: 'https://api.sonyliv.com/api/v1/xdr',
            headers: {
                'Content-Type': 'application/json',
                'x-via-device': 'true',
                'Authorization': access_token,
            },
            method: 'POST',
            dataType: 'json',
            data: JSON.stringify(request_json),
            error: function (jqXHR, status, error) {
                if (jqXHR.status == 400) {
                    alert("Sorry, ssssss Please check your input information ");

                } else {
                    alert("An error in cw occurred: " + status + "nError: " + error);
                }
            },
            success: function (data, status) {
              
            }


        });
    }
}

function deleteContinuesWatchingData(assetID){
    var access_token = localStorage.getItem("access_token");
    if (access_token) {
     $.ajax({
            url: 'https://api.sonyliv.com/api/v1/xdr/'+assetID,
            headers: {
                'Content-Type': 'application/json',
                'x-via-device': 'true',
                'Authorization': access_token,
            },
            method: 'DELETE',
            error: function (jqXHR, status, error) {
                if (jqXHR.status == 400) {
                    alert("Delete Success");

                } else {
                    alert("An error ssssss occurred in : deleteContinuesWatchingData" + status + "nError: " + error);
                }
            },
            success: function (data, status) {
              
            }


        });
    }
}

