'use strict';
var class_name;
var pageCount = 0;
var videosArray = localStorage.getItem('videos') ? JSON.parse(localStorage.getItem('videos')) : [];
var CURRENT_SELECTED_MENU=localStorage.getItem('CURRENT_FOCUS_ITEM');

var PREMIUM_ICONS_URL='../img/icon/premium1.png';
//alert(CURRENT_SELECTED_MENU)
if(CURRENT_SELECTED_MENU != "home" && CURRENT_SELECTED_MENU != "shows" && CURRENT_SELECTED_MENU != "movies" && CURRENT_SELECTED_MENU != "sports" && CURRENT_SELECTED_MENU != "premium" && CURRENT_SELECTED_MENU != "channel" && CURRENT_SELECTED_MENU == null){
    //&& CURRENT_SELECTED_MENU == null
    CURRENT_SELECTED_MENU = "home";
    localStorage.setItem('CURRENT_FOCUS_ITEM' ,CURRENT_SELECTED_MENU);
    // localStorage.removeItem("CURRENT_SLIDER");
    // localStorage.removeItem("CURRENT_SLIDER_ELEMENT");
}
if(CURRENT_SELECTED_MENU=='account') {
   CURRENT_SELECTED_MENU = "home";
   localStorage.setItem('CURRENT_FOCUS_ITEM',CURRENT_SELECTED_MENU)
}
if(CURRENT_SELECTED_MENU=='search') {
   CURRENT_SELECTED_MENU = "home";
   localStorage.setItem('CURRENT_FOCUS_ITEM',CURRENT_SELECTED_MENU)
}

var FIRST_TIME_CALL_BOTTOM = 1;
var ref_var = '';
var ASSETS_ITEM_ID_WITH_SLIDER='';
function handler(e) {

    if (e.keyCode == 40) { //bottom
    
        scrollToLeftEnable = 0;
        $('#menuTitle').html('');
        if (FIRST_TIME_CALL_BOTTOM == 1 && IS_DETAILS_PAGE == 0) {
            FIRST_TIME_CALL_BOTTOM++;
            $('.rent').css('visibility', 'hidden');
            $('.sony-logo').css('visibility', 'hidden');
            $('.search').css('visibility', 'hidden');
            document.getElementById('slider1').scrollIntoView();
            $("#slider1").focus();

        } else {
            FIRST_TIME_CALL_BOTTOM++;
            ref_var = $('a:focus').parent().parent().parent().next().next().find('li:first');
            if (ref_var.length) {
                ref_var.focus();
                var currentID = $('a:focus').parent().parent().parent().attr('id');
                document.getElementById(currentID).scrollIntoView();
            }

        }
        // alert($('a:focus').parent().attr('premium'));
        if ($('a:focus').parent().attr('premium') == 1) {
            $('.rent img').attr('src', PREMIUM_ICONS_URL);
            $('.rent').css('visibility', 'visible');
        } else if ($('a:focus').parent().attr('premium') == 2) {
            $('.rent img').attr('src', '../img/icon/rent.png');
            $('.rent').css('visibility', 'visible');
        } else {
            $('.rent').css('visibility', 'hidden');
        }
        $('#home-page-title').html($('a:focus').parent().attr('poster_title'));
        $('#home-page-state').html($('a:focus').parent().data('title-static'));
        $('#home-page-description').html($('a:focus').parent().attr('poster_desc'));
        $('#slider1 ul').animate({
            'left': ""
        });
    } else if (e.keyCode == 38) { //top
        scrollToLeftEnable = 0;
        FIRST_TIME_CALL_BOTTOM--;
        if ($('a:focus').parent().parent().parent().attr('id') == "slider2") {
            $('.rent').css('visibility', 'hidden');
            $('.sony-logo').css('visibility', 'visible');
            $('.search').css('visibility', 'visible');
            var name = $('a:focus').attr("title");
            $('#menuTitle').html(name);
            $('#home-page-title').html('');
            $('#home-page-description').html('');
            $('#home-page-state').html('');

            document.getElementById('slider1').scrollIntoView();
            $('#slider1 li:first a').focus();
        } else {

            var currentID = $('a:focus').parent().parent().parent().prev().prev().attr('id');
               $('#menuTitle').html(name);
       
            if (currentID.length) {
                document.getElementById(currentID).scrollIntoView();
            }

            if ($('a:focus').parent().attr('premium') == 1) {
                $('.rent img').attr('src', PREMIUM_ICONS_URL);
                $('.rent').css('visibility', 'visible');
            } else if ($('a:focus').parent().attr('premium') == 2) {
                $('.rent img').attr('src', '../img/icon/rent.png');
                $('.rent').css('visibility', 'visible');
            } else {
                $('.rent').css('visibility', 'hidden');
            }

        }

                 $('#home-page-title').html($('a:focus').parent().attr('poster_title'));
        $('#home-page-state').html($('a:focus').parent().data('title-static'));
        $('#home-page-description').html($('a:focus').parent().attr('poster_desc'));
        
        $('#slider1 ul').animate({
            'left': ""
        });
    }
    var finalImage = $('a:focus img').data('img');
    if ($('a:focus img').data('imgtype') == 'bgImg') {
        $('#home-page-poster').css('background-image', 'url("../img/gradient-copy.png"),url("' + finalImage + '") ');
        $('#home-page-poster').css('background-size', '80%, 80%');

    } else if ($('a:focus  img').data('imgtype') == 'thumbnailImg') {
        $('#home-page-poster').css('background-image', 'url("../img/gradient-copy.png"), url("' + finalImage + '")');
        $('#home-page-poster').css('background-size', '80% 80%, 70% 80%');
        $('#home-page-poster').css('background-position', 'right top, right top');

    } else {
        $('#home-page-poster').css('background-image', 'url("../img/Home_menu_bg.png"), url("../img/gradient-copy.png")');
        $('#home-page-poster').css('background-size', '80%, 80%');

    }
    if($('a:focus').parent().data('cw')){
        $('#progressDisplay').html('<div class="progressbar"><div style="width: '+$('a:focus').parent().data('progress')+'%;"></div></div>');
    } else{
        $('#progressDisplay').html('');
    }
    //$('#home-page-title').html('<h1>====='+tempData+'</h1>');
}


$(document).ready(function () {
    $('#loading-overlay').show();
    localStorage.removeItem('detailsToPlayVideo');
    localStorage.removeItem('premium_redirect');
    function S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }

    guid = (S4() + S4() + S4() + S4()).toLowerCase();

    var access_token = localStorage.getItem("access_token");

    if (access_token) {
        // alert("call access token");
        var getAllSubscription_data = {
            "channelPartnerID": "MSMIND1",
        }
        var Subscription_data = JSON.stringify(getAllSubscription_data);


        console.log("access_token" + access_token);
        /* start ajax submission process */
        $.ajax({
            url: 'https://api.sonyliv.com/api/subscription/getAllSubscriptions',
            headers: {
                'Content-Type': 'application/json',
                'x-via-device': 'true',
                //'Authorization' :"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIwMTYwODE2MDYwNzAyODIwIiwidG9rZW4iOiJMRHZuLVZqUUktSng2Yi1jN2RILThYcG8tSlpCRy01YSIsImV4cGlyYXRpb25Nb21lbnQiOiIyMDE5LTExLTI5VDA0OjAzOjA3LjQ3OFoiLCJpc1Byb2ZpbGVDb21wbGV0ZSI6InRydWUiLCJjaGFubmVsUGFydG5lcklEIjoiTVNNIiwiZmlyc3ROYW1lIjoiS2FrZXNoIE5hcmF5YW4iLCJlbWFpbCI6Imdlbml1c2tha2VzaEBnbWFpbC5jb20iLCJzZXNzaW9uQ3JlYXRpb25UaW1lIjoiMjAxOC0xMC0wOFQxMzowMzowNy42NzRaIiwiaWF0IjoxNTM5MDAzNzg3LCJleHAiOjE1NzUwMDAxMjd9.7sJizbiUJJhklnNMY44NCZEFOKRD2saayvsRCk56xro",
                'Authorization': access_token,

            },
            method: 'POST',
            dataType: 'json',
            data: Subscription_data,
            error: function (jqXHR, status, error) {
                if (jqXHR.status == 400) {
                    //alert("Sorry, Please check your input information ");

                } else {
                    //alert("An error occurred: " + status + "nError: " + error);
                }
            },
            success: function (data, status) {
                //alert(data);
                console.log('getAllSubscriptions: ' + JSON.stringify(data));

                if (status == "success") {
                    var RENT_SUBSCRIPTIONS = [];
                    for (var activeSubscriptions = 0; activeSubscriptions < data.activeSubscriptions.length; activeSubscriptions++) {
                        console.log('getAllSubscriptions: ' + JSON.stringify(data.activeSubscriptions[activeSubscriptions].startDate));
                        console.log('getAllSubscriptions: ' + data.activeSubscriptions[activeSubscriptions].startDate);
                        var duration = parseInt(data.activeSubscriptions[activeSubscriptions].validityTill);
                        //alert("duration"+data.activeSubscriptions[activeSubscriptions].validityTill);
                        //  var d = new Date(duration);
                        activeSubscriptions_date = new Date(duration);
                        console.log("d" + activeSubscriptions_date);
                        PREMIUM_ICONS_URL='../img/icon/ic_premium_unlocked.png';

                        if(data.activeSubscriptions[activeSubscriptions].subscriptionType=='TVOD' && data.activeSubscriptions[activeSubscriptions].assets)
                        {
                            RENT_SUBSCRIPTIONS.push(data.activeSubscriptions[activeSubscriptions].assets[0].assetId); 
                        }
                    }
                    localStorage.setItem('RENT_SUBSCRIPTIONS', JSON.stringify(RENT_SUBSCRIPTIONS));
		    sendXdrRequest();

                }
                asset_band_details();
                session();
                getDMADetails();
            }



        });

    } else {
        session();
    }
    window.addEventListener("keypress", handler);
    window.addEventListener("keydown", handler);

    if(localStorage.getItem("CURRENT_SLIDER")==undefined && localStorage.getItem("CURRENT_SLIDER_ELEMENT")==undefined && CURRENT_SELECTED_MENU!=undefined) {
        $("#"+CURRENT_SELECTED_MENU+"_menu a").focus();
    }

    $(this).keyup(function (e) {
        var $this = $("a:focus");
        var elemNum = $($this).data('num');
        var type = $('a:focus').data("type");
        var activeSliderId = $this.parent().parent().parent().attr('id');
        if (e.keyCode == 37) { //left
            if (type != 'menu') {
                moveLeft("#" + activeSliderId);
            }
            $('#home-page-title').html($this.parent().attr('poster_title'));
            $('#home-page-state').html($('a:focus').parent().data('title-static'));
            $('#home-page-description').html($this.parent().attr('poster_desc'));
            if ($this.parent().attr('premium') == 1) {
                $('.rent img').attr('src', PREMIUM_ICONS_URL);
                $('.rent').css('visibility', 'visible');
            } else if ($this.parent().attr('premium') == 2) {
                $('.rent img').attr('src', '../img/icon/rent.png');
                $('.rent').css('visibility', 'visible');
            } else {
                $('.rent').css('visibility', 'hidden');
            }
            if (type == 'menu') {
                var name = $('a:focus').attr("title");
                $('#menuTitle').html(name);
                $('#home-page-title').html('');
                $('#home-page-description').html('');
                   $('#home-page-state').html('');
            } else {
                $('#menuTitle').html('');
            }
        } else if (e.keyCode == 39) { // right
            var isPaginationRequired = $this.parent().data('pagination');
            if (isPaginationRequired) {
                var showNext = ($this.parent().data('vcount') + 1) % 10;
                if (!showNext) {
                    var pageToBe = (($this.parent().data('vcount') + 1) / 10);
                    var sCount = $this.parent().data('scount');
                    var bandData = $this.parent().data('banddata');
                    var band = $this.parent().data('band');
                    var vidType = $this.parent().data('vtype');
                    getNextPageRecord(pageToBe, sCount, bandData, band, vidType);
                }
            }
            if (type != 'menu') {
                moveRight("#" + activeSliderId);
            }
            $('#home-page-title').html($this.parent().attr('poster_title'));
            $('#home-page-state').html($('a:focus').parent().data('title-static'));
            $('#home-page-description').html($this.parent().attr('poster_desc'));
            if ($this.parent().attr('premium') == 1) {
                if (IS_DETAILS_PAGE == 1) {
                    $('.rent img').attr('src', PREMIUM_ICONS_URL);

                } else {
                    $('.rent img').attr('src', PREMIUM_ICONS_URL);
                }
                $('.rent').css('visibility', 'visible');
            } else if ($this.parent().attr('premium') == 2) {
                if (IS_DETAILS_PAGE == 1) {
                    $('.rent img').attr('src', '../img/icon/rent.png');

                } else {
                    $('.rent img').attr('src', '../img/icon/rent.png');
                }
                $('.rent').css('visibility', 'visible');
            } else {
                $('.rent').css('visibility', 'hidden');
            }

            if (type == 'menu') {
                var name = $('#slider1 a:focus').attr("title");
                $('#menuTitle').html(name);
                $('#home-page-title').html('');
                $('#home-page-description').html('');
                   $('#home-page-state').html('');
            } else {
                $('#menuTitle').html('');
            }
        } else if (e.keyCode == 40) { //bottom
            scrollToLeftEnable = 0;
            if (FIRST_TIME_CALL_BOTTOM == 1 && IS_DETAILS_PAGE == 0) {
                FIRST_TIME_CALL_BOTTOM++;
                $('.rent').css('visibility', 'hidden');
                $('.sony-logo').css('visibility', 'hidden');
                $('.search').css('visibility', 'hidden');
                document.getElementById('slider2').scrollIntoView();
            } else {
                FIRST_TIME_CALL_BOTTOM++;
                ref_var = $('a:focus').parent().parent().parent().next().next().find('li:first');
                if (ref_var.length) {
                    var currentID = $('a:focus').parent().parent().parent().attr('id');
                    document.getElementById(currentID).scrollIntoView();
                }

            }
            if ($('a:focus').parent().attr('premium') == 1) {
                $('.rent img').attr('src', PREMIUM_ICONS_URL);
                $('.rent').css('visibility', 'visible');
            } else if ($('a:focus').parent().attr('premium') == 2) {
                $('.rent img').attr('src', '../img/icon/rent.png');
                $('.rent').css('visibility', 'visible');
            } else {
                $('.rent').css('visibility', 'hidden');
            }
            $('#home-page-title').html($('a:focus').parent().attr('poster_title'));
            $('#home-page-state').html($('a:focus').parent().data('title-static'));
            $('#home-page-description').html($('a:focus').parent().attr('poster_desc'));
            $('#slider1 ul').animate({
                'left': ""
            });
        } else if (e.keyCode == 13) {
            var type = $('a:focus').data("type");
            if (type == 'menu') {
                var name = $('a:focus').attr("title").toLowerCase();
                localStorage.setItem('CURRENT_FOCUS_ITEM',name);
                localStorage.removeItem("CURRENT_SLIDER");
                localStorage.removeItem("CURRENT_SLIDER_ELEMENT");
                shows(name);
            } else {
                var smode = $('a:focus').parent().data('smode');
                if (smode == 'SVOD' || smode == 'TVOD') {
                    homebackground($('a:focus').parent().data('turl'));
                } else {
                    var id = $('a:focus').parent().data('id');
                    var vid = $('a:focus').parent().data('vid');
                    var classification = $('a:focus').parent().data('classification');
                    var vtype = $('a:focus').parent().data('vtype');
                    homebackground(id, vid, classification, vtype);
                }
            }
        }
        var finalImage = $('a:focus img').data('img');
        if ($('a:focus img').data('imgtype') == 'bgImg') {
            $('#home-page-poster').css('background-image', 'url("../img/gradient-copy.png"),url("' + finalImage + '") ');
            $('#home-page-poster').css('background-size', '80%, 80%');

        } else if ($('a:focus  img').data('imgtype') == 'thumbnailImg') {
            $('#home-page-poster').css('background-image', 'url("../img/gradient-copy.png"), url("' + finalImage + '")');
            $('#home-page-poster').css('background-size', '80% 80%');
            $('#home-page-poster').css('background-position', 'right top, right top');

        } else {
            $('#home-page-poster').css('background-image', 'url("../img/Home_menu_bg.png"), url("../img/gradient-copy.png")');
            $('#home-page-poster').css('background-size', '80%, 80%');
        }
        if($('a:focus').parent().data('cw')){
            $('#progressDisplay').html('<div class="progressbar"><div style="width: '+$('a:focus').parent().data('progress')+'%;"></div></div>');
        } else{
            $('#progressDisplay').html('');
        }
        // $('#home-page-title').html('<h1>'+e.keyCode+'</h1>');

    });
    //shows("home");
});

var guid;
var myResponse;
var searchstringArr = [];
var config_nav_globalX;
var accdeo_global_data;
var config_bands_globalx;
var vodSearch;

function session() {
    var seesion_data = {
        "uuid": guid,
        // "appKey": "5b069b37a0e845000c29711a"
        "appKey": "5bbd962e1de1c4001a276425"

    }
    $.ajax({
        // url: baseURL +'api/v3/auth/login',
        url: 'https://api.one.accedo.tv/session',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'x-via-device': 'true',
        },
        method: 'GET',
        dataType: 'json',
        data: seesion_data,
        error: function (jqXHR, status, error) {
            if (jqXHR.status == 400) {
                // alert("Sorry, Please check your input information ");
            }
            if (jqXHR.status == 500) {
                // alert("Internal server error ");
            } else {
                // alert("An error occurred: " + status + "nError: " + error);
            }
        },
        success: function (data, status) {
            if (status == "success") {
                var seesionV = data.sessionKey;
                var expirationT = data.expiration;
                var titleStrArr = {};
                $.ajax({

                    url: 'https://api.one.accedo.tv/metadata?sessionKey=' + seesionV,
                    headers: {
                        // 'Content-Type': 'application/x-www-form-urlencoded',
                        // 'x-via-device': 'true',
                    },
                    method: 'GET',
                    dataType: 'json',
                    error: function (jqXHR, status, error) {
                        if (jqXHR.status == 400) {
                            //  alert("Sorry, Please check your input information ");
                        }
                        if (jqXHR.status == 500) {
                            // alert("Internal server error ");
                        } else {
                            //  alert("An error occurred: " + status + "nError: " + error);
                        }
                    },
                    success: function (data, status) {
                        localStorage.setItem('policy_key', data.config_gateways.cfg_setting_brightcove.bolt_account.policy_key);
                        localStorage.setItem('bolt_account', JSON.stringify(data.config_gateways.cfg_setting_brightcove.bolt_account));
                        localStorage.setItem('studio_account', JSON.stringify(data.config_gateways.cfg_setting_brightcove.studio_account));
                        localStorage.setItem('playback_quality_cfg', JSON.stringify(data.config_app_settings.cfg_app_player.playback_quality_cfg));
                        console.log("hi" + JSON.stringify(data));
                        if (status == "success") {
                            accdeo_global_data = data;
                            //alert(CURRENT_SELECTED_MENU)
                            shows(CURRENT_SELECTED_MENU);
                            for (var m = 0; m < data.config_nav.length; m++) {
                                var menuName = data.config_nav[m].id;
                                config_nav_globalX = data.config_nav;
                                for (var n = 0; n < data.config_nav[m].items.length; n++) {
                                    var menuName = data.config_nav[m].items[n].action.replace("sony://page/", "");
                                    for (var i = 0; i < data.config_pages.length; i++) {
                                        var config_pages_menuName = data.config_pages[i].id;
                                        if (menuName == config_pages_menuName) {
                                            for (var j = 0; j < data.config_pages[i].bands.length; j++) {
                                                for (var k = 0; k < data.config_bands.length; k++) {
                                                    if (data.config_pages[i].bands[j].id == data.config_bands[k].id) {
                                                        config_bands_globalx = data.config_bands;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }
    });
}
var configpages_bands = [];
var configbands = [];
var showchstringArr = [];
var titlenamev = [];
var titlenamevalue;
var assetsdata = [];

var slideCount = $('#' + class_name + ' ul li').length;

var slideWidth = $('#' + class_name + ' ul li').width();
var slideHeight = $('#' + class_name + ' ul li').height();
var sliderUlWidth = slideCount * slideWidth;
var scrollToLeftEnable = 0;
var activeSliderId = $('a:focus').parent().parent().parent().attr('id');
class_name = activeSliderId;
$('#' + class_name).css({
    "width": "100%",
});

$('#' + class_name + ' ul').css({
    width: sliderUlWidth,
    marginLeft: -slideWidth
});

function moveLeft(idName) {
    $(idName + ' ul').animate({
        left: +slideWidth
    }, 10, function () {
        $(idName + ' ul li:last-child').prependTo(idName + ' ul');
        $(idName + ' ul').css('left', '');
    });
};

function moveRight(idName) {
    //var current_selected_element=$('.active').attr('data-vcount');
    //alert(1);
    scrollToLeftEnable = 1;
    $(idName + ' ul').animate({
        left: "+=" + slideWidth
    }, 10, function () {
        $(idName + ' ul li:first-child').appendTo(idName + ' ul');
        $(idName + ' ul').css('left', "+=" + slideWidth);
    });

};
 var menubar_event;
function shows(show_type) {
    menubar_event = show_type;
    $('#slider').empty();
    $('#slider').html('');
    //alert(menubar_event)
    if (menubar_event == "shows") {
        $('#home-page-poster').css('background-image', 'url("../img/placeholder_shows.jpg") , url("../img/gradient-copy.png")');
        $('#home-page-poster').css('background-size', '80%, 80%');

        $('#slider').empty();
        configpages_bands = [];
        showchstringArr = [];
        configbands = [];
        for (var i = 0; i < accdeo_global_data.config_pages.length; i++) {
            if (accdeo_global_data.config_pages[i].id == "ALL_SHOW_PAGE") {
                for (var bands_i = 0; bands_i < accdeo_global_data.config_pages[i].bands.length; bands_i++) {
                    var configband_idsearchData = {
                        "id": accdeo_global_data.config_pages[i].bands[bands_i].id
                    }
                    configpages_bands.push(configband_idsearchData);
                }
            }
        }
    } else if (menubar_event == "movies") {
        $('#home-page-poster').css('background-image', 'url("../img/placeholder_shows.jpg") , url("../img/gradient-copy.png")');
        $('#home-page-poster').css('background-size', '80%, 80%');

        configpages_bands = [];
        showchstringArr = [];
        configbands = [];
        $('#slider').empty();
        for (var i = 0; i < accdeo_global_data.config_pages.length; i++) {
            if (accdeo_global_data.config_pages[i].id == "ALL_MOVIE_PAGE") {
                for (var bands_i = 0; bands_i < accdeo_global_data.config_pages[i].bands.length; bands_i++) {
                    var configband_idsearchData = {
                        "id": accdeo_global_data.config_pages[i].bands[bands_i].id
                    }
                    configpages_bands.push(configband_idsearchData);
                }
            }
        }
    } else if (menubar_event == "home") {
        $('#home-page-poster').css('background-image', 'url("../img/home_backgroung.jpeg") , url("../img/gradient-copy.png")');
        $('#home-page-poster').css('background-size', '80%, 80%');

        configpages_bands = [];
        showchstringArr = [];
        configbands = [];
        $('#slider').empty();
        for (var i = 0; i < accdeo_global_data.config_pages.length; i++) {
            if (accdeo_global_data.config_pages[i].id == "SONYHOME") {
                for (var bands_i = 0; bands_i < accdeo_global_data.config_pages[i].bands.length; bands_i++) {
                    var configband_idsearchData = {
                        "id": accdeo_global_data.config_pages[i].bands[bands_i].id
                    }
                    configpages_bands.push(configband_idsearchData);
                }
            }
        }
    } else if (menubar_event == "sports") {
        $('#home-page-poster').css('background-image', 'url("../img/home_backgroung.jpeg") , url("../img/gradient-copy.png")');
        $('#home-page-poster').css('background-size', '80%, 80%');

        configpages_bands = [];
        showchstringArr = [];
        configbands = [];
        $('#slider').empty();
        for (var i = 0; i < accdeo_global_data.config_pages.length; i++) {
            if (accdeo_global_data.config_pages[i].id == "ALL_SPORT_PAGE") {
                for (var bands_i = 0; bands_i < accdeo_global_data.config_pages[i].bands.length; bands_i++) {
                    var configband_idsearchData = {
                        "id": accdeo_global_data.config_pages[i].bands[bands_i].id
                    }
                    configpages_bands.push(configband_idsearchData);
                }
            }
        }
    } else if (menubar_event == "premium") {
        $('#home-page-poster').css('background-image', 'url("../img/placeholder_shows.jpg") , url("../img/gradient-copy.png")');
        $('#home-page-poster').css('background-size', '80%, 80%');

        configpages_bands = [];
        showchstringArr = [];
        configbands = [];
        $('#slider').empty();
        for (var i = 0; i < accdeo_global_data.config_pages.length; i++) {
            if (accdeo_global_data.config_pages[i].id == "PREMIUM_OFFERINGS") {
                for (var bands_i = 0; bands_i < accdeo_global_data.config_pages[i].bands.length; bands_i++) {
                    var configband_idsearchData = {
                        "id": accdeo_global_data.config_pages[i].bands[bands_i].id
                    }
                    configpages_bands.push(configband_idsearchData);
                }
            }
        }
    } else if (menubar_event == "channel") {
        $('#home-page-poster').css('background-image', 'url("../img/home_backgroung.jpeg") , url("../img/gradient-copy.png")');
        $('#home-page-poster').css('background-size', '80%, 80%');

        configpages_bands = [];
        showchstringArr = [];
        configbands = [];
        $('#slider').empty();
        for (var i = 0; i < accdeo_global_data.config_pages.length; i++) {
            if (accdeo_global_data.config_pages[i].id == "LIVE_CHANNELS_PAGE") {
                for (var bands_i = 0; bands_i < accdeo_global_data.config_pages[i].bands.length; bands_i++) {
                    var configband_idsearchData = {
                        "id": accdeo_global_data.config_pages[i].bands[bands_i].id
                    }
                    configpages_bands.push(configband_idsearchData);
                }
            }
        }
    } else if (menubar_event.toLowerCase() == "account") {
       // alert(activeSubscriptions_date);
         var access_token = localStorage.getItem("access_token");
         // alert("access_token"+access_token);
         // alert(activeSubscriptions_date);
         if(access_token){
            window.location.href="myaccount.html";
             if (activeSubscriptions_date) {
             window.location.href="myaccount.html";
             }
         }

       else{
           window.location.href="login.html"; 
        }
        
        //alert(1);
        return true;
    } else if (menubar_event.toLowerCase() == "settings") {
        window.location.href="settings.html";
        return true;
    } else if (menubar_event.toLowerCase() == "more") {
        window.location.href="privacy.html";
        return true;
    } else if (menubar_event.toLowerCase() == "search") {
        //sessionStorage.clear();
        localStorage.removeItem('search_t');
         localStorage.removeItem('inputvalue');
        window.location.href="search.html";
        return true;
    }

    for (var config_bands_i = 0; config_bands_i < accdeo_global_data.config_bands.length; config_bands_i++) {
        if (config_bands_i.id == "xdr") {
           var band_idsearchData = {
           "action": "",
           "data": accdeo_global_data.config_bands[config_bands_i].data,
           "id": accdeo_global_data.config_bands[config_bands_i].id,
           "pageNumber": "0",
           "pageSize": "10",
           "sortOrder": accdeo_global_data.config_bands[108].sort,
           "type": accdeo_global_data.config_bands[config_bands_i].type
       }       
               configbands.push(band_idsearchData);

        } else {
            var band_idsearchData = {
           "action": "",
           "data": accdeo_global_data.config_bands[config_bands_i].data,
           "id": accdeo_global_data.config_bands[config_bands_i].id,
           "pageNumber": "0",
           "pageSize": "10",
           "sortOrder": accdeo_global_data.config_bands[config_bands_i].sort,
           "type": accdeo_global_data.config_bands[config_bands_i].type
        }
                configbands.push(band_idsearchData);
        }
        // var band_idsearchData = {
        //     "action": "",
        //     "data": accdeo_global_data.config_bands[config_bands_i].data,
        //     "id": accdeo_global_data.config_bands[config_bands_i].id,
        //     "pageNumber": "0",
        //     "pageSize": "10",
        //     "sortOrder": accdeo_global_data.config_bands[config_bands_i].sort,
        //     "type": accdeo_global_data.config_bands[config_bands_i].type
        // }
        // configbands.push(band_idsearchData);
    }
    var CURRENT_SLIDER_ELEMENT_BAND = localStorage.getItem("CURRENT_SLIDER_ELEMENT_BAND");
    var pageSize = 10;
    for (var a = 0; a < configpages_bands.length; a++) {
        for (var b = 0; b < configbands.length; b++) {
            if (configpages_bands[a].id == configbands[b].id) {
                if(CURRENT_SLIDER_ELEMENT_BAND!=undefined && CURRENT_SLIDER_ELEMENT_BAND!='' && CURRENT_SLIDER_ELEMENT_BAND==configbands[b].id) {
                    pageSize = Math.ceil((localStorage.getItem("CURRENT_SLIDER_ELEMENT")+1)/10);
                    pageSize = pageSize*10;

                    //alert(curPage+'_'+pageNumber)
                } else {
                    pageSize = 10;
                }
                let showsearchData = {
                    "action": "",
                    "data": configbands[b].data,
                    "id": configbands[b].id,
                    "pageNumber": "0",
                    "pageSize": pageSize,
                    "sortOrder": configbands[b].sort,
                    "type": configbands[b].type
                };
                showchstringArr.push(showsearchData);
            }
        }
    }

    var showQueryData = {
        "detailsType": "basic",
        "deviceDetails": {
            "deviceId": "71066286297",
            "mfg": "GoogleChrome",
            "model": "GoogleChrome",
            "os": "Android",
            "osVer": "XXX"
        },
        "isSearchable": true,
        "searchSet": showchstringArr
    }

    var access_token = localStorage.getItem("access_token");

    $.ajax({
        url: 'https://api.sonyliv.com/api/v4/vod/search',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'x-via-device': 'true',
            'Authorization': access_token,
            // 'Access-Control-Allow-Origin': 'https://www.sonyliv.com/',
            // "Access-Control-Allow-Origin" : "*",
        },
        method: 'POST',
        dataType: 'json',
        data: showQueryData,
        error: function (jqXHR, status, error) {
            if (jqXHR.status == 400) {
                //alert("Sorry, Please check your input information ");
            }
            if (jqXHR.status == 500) {
                // alert("Internal server error ");
            } else {
                // alert("An error occurred: " + status + "nError: " + error);
            }
            $('#loading-overlay').hide();
        },
        success: function (data, status) {

            if (status == "success") {
                $('#loading-overlay').hide();
                //                console.log("daata" + JSON.stringify(data));
                //
                //                console.log(data);
                var current_date = new Date();
                /// alert("current_date"+ current_date);
                console.log("activeSubscriptions_date" + activeSubscriptions_date);
                //if(current_date !== activeSubscriptions_date){
                if (activeSubscriptions_date || localStorage.getItem('access_token')) {
                    //alert("false");
                    var pidx = 1;
                    for (var b = 0; b < config_bands_globalx.length; b++) {
                        for (var p = 0; p < data.length; p++) {
                            let index_title = data[p].id;
                            titlenamevalue = config_bands_globalx[b].title;
                            //if (index_title == config_bands_globalx[b].id && ((titlenamevalue != 'Continue Watching' && videosArray.length) || data[p].assets.length)) {
                            //if (index_title == config_bands_globalx[b].id && ((titlenamevalue != 'Continue Watching' && videosArray.length) || (data[p].assets.length))) {
                            if (index_title == config_bands_globalx[b].id && ( data[p].hasOwnProperty('assets') && data[p].assets.length)) {
                                pidx++

                                titlenamev.push(config_bands_globalx[b].title);
                                $("#slider").append('<p > ' + titlenamevalue + '</p>');
                                class_name = 'slider' + pidx;
                                var ulBody = "<div id=" + class_name + " class=\"test_name\"><ul class='item-ul'>";
                               // if (titlenamevalue != 'Continue Watching') 
                {
                                    for (var q = 0; q < data[p].assets.length; q++) {
                                        // if (data[p].assets.length !== 0) {
                                        var data_title_static = '';
                                        if (data[p].assets[q].type) {
                                            data_title_static += data[p].assets[q].type;
                                        }
                                        if (data[p].assets[q].genre) {
                                            data_title_static += '<span> . </span>' + data[p].assets[q].genre;
                                        }

                                        if (data[p].assets[q].classification) {
                                            data_title_static += '<span> . </span>' + data[p].assets[q].classification;
                                        }
                                          ASSETS_ITEM_ID_WITH_SLIDER=class_name+"_"+q;
                                          if(!data[p].assets[q].assetLandscapeImage){
                                             data[p].assets[q].assetLandscapeImage = data[p].assets[q].thumbnailUrl;

                                            }

                                        if (data[p].assets[q].posterUrl != "null" && data[p].assets[q].posterUrl != "undefined" && data[p].assets[q].posterUrl != "" ) {
                                            data[p].assets[q].tvBackgroundImage = data[p].assets[q].posterUrl;
                                        }else if (data[p].assets[q].assetLandscapeImage != "null" && data[p].assets[q].assetLandscapeImage != "undefined" && data[p].assets[q].assetLandscapeImage != "" ) {
                                            data[p].assets[q].tvBackgroundImage = data[p].assets[q].assetLandscapeImage;
                                        }else {
                                            data[p].assets[q].tvBackgroundImage = data[p].assets[q].thumbnailUrl;
                                            imgType = 'thumbnailImg';
                                        }
                                       


                                        data[p].assets[q].thumbnailUrl = "https://resources.sonyliv.com/image/fetch/h_147,w_260,c_fill,fl_lossy,f_auto,q_auto,e_contrast:30,e_brightness:10/" + data[p].assets[q].assetLandscapeImage;

                                        var imgType = 'bgImg';
                                        if (!data[p].assets[q].tvBackgroundImage) {
                                            data[p].assets[q].tvBackgroundImage = data[p].assets[q].thumbnailUrl;
                                            imgType = 'thumbnailImg';
                                        }
                                        if (data[p].assets[q].subscriptionMode == 'Free') {
                                            ulBody += '<li id="'+ASSETS_ITEM_ID_WITH_SLIDER+'" premium="0" data-pagination="true" data-title-static="' + data_title_static + '" data-vcount="' + (q + 1) + '" data-scount="' + pidx + '" data-band="' + index_title + '" data-banddata="' + config_bands_globalx[b].data + '" data-vtype="' + config_bands_globalx[b].type + '" poster_title="' + data[p].assets[q].title + '" poster_desc="' + data[p].assets[q].shortDesc + '"><a  onclick="homebackground(\'' + data[p].assets[q].id + '\', \'' + data[p].assets[q].vid + '\',\'' + data[p].assets[q].classification + '\', \'' + data[p].assets[q].type + '\' , \'' + data[p].assets[q].subscriptionMode + '\');"  href="javascript:void(0);" ><img data-imgtype="' + imgType + '" data-img="' + data[p].assets[q].tvBackgroundImage + '" src= ' + data[p].assets[q].thumbnailUrl + ' /></a></li>';
                                            assetsdata.push(data[p].assets[q].thumbnailUrl);
                                        } else if (data[p].assets[q].subscriptionMode == "SVOD") {
                                            var premiumImg = (localStorage.getItem('isUserPremium')!=null && (localStorage.getItem('isUserPremium')=='true')) ? '../img/icon/ic_premium_unlocked.png':'../img/icon/premium1.png';
                                            ulBody += '<li id="'+ASSETS_ITEM_ID_WITH_SLIDER+'" premium="1" data-pagination="true"  data-title-static="' + data_title_static + '"  data-vcount="' + (q + 1) + '" data-scount="' + pidx + '" data-band="' + index_title + '" data-banddata="' + config_bands_globalx[b].data + '" data-vtype="' + config_bands_globalx[b].type + '" poster_title="' + data[p].assets[q].title + '" poster_desc="' + data[p].assets[q].shortDesc + '" > <span><img data-imgtype="' + imgType + '" data-img="' + data[p].assets[q].tvBackgroundImage + '" src="'+premiumImg+'"></span><a  onclick="homebackground(\'' + data[p].assets[q].id + '\', \'' + data[p].assets[q].vid + '\',\'' + data[p].assets[q].classification + '\', \'' + data[p].assets[q].type + '\' , \'' + data[p].assets[q].subscriptionMode + '\');" href="javascript:void(0);" > <img  data-imgtype="' + imgType + '" data-img="' + data[p].assets[q].tvBackgroundImage + '" src= ' + data[p].assets[q].thumbnailUrl + ' /></a></li>';
                                        } else if (data[p].assets[q].subscriptionMode == "TVOD") {

                                            var rentImg = (localStorage.getItem('RENT_SUBSCRIPTIONS')!=null && (localStorage.getItem('RENT_SUBSCRIPTIONS')).indexOf(data[p].assets[q].id)>-1) ? '../img/icon/ic_rented.png':'../img/icon/rent.png';

                                            ulBody += '<li id="'+ASSETS_ITEM_ID_WITH_SLIDER+'" premium="2" data-pagination="true"  data-title-static="' + data_title_static + '"  data-vcount="' + (q + 1) + '" data-scount="' + pidx + '" data-band="' + index_title + '" data-banddata="' + config_bands_globalx[b].data + '" data-vtype="' + config_bands_globalx[b].type + '" poster_title="' + data[p].assets[q].title + '" poster_desc="' + data[p].assets[q].shortDesc + '" ><span><img data-imgtype="' + imgType + '" data-img="' + data[p].assets[q].tvBackgroundImage + '" src="'+rentImg+'"></span><a  onclick="homebackground(\'' + data[p].assets[q].id + '\', \'' + data[p].assets[q].vid + '\',\'' + data[p].assets[q].classification + '\', \'' + data[p].assets[q].type + '\' , \'' + data[p].assets[q].subscriptionMode + '\');" href="javascript:void(0);" > <img  data-imgtype="' + imgType + '" data-img="' + data[p].assets[q].tvBackgroundImage + '" src= ' + data[p].assets[q].thumbnailUrl + ' /></a></li>';
                                        }
                                        // }
                                    }

                                } 
                /*
                else {
                                    if (videosArray.length) {
                                        for (var v = 0; v < videosArray.length; v++) {

                                            if(!videosArray[v].assetLandscapeImage){
                                             videosArray[v].assetLandscapeImage = videosArray[v].thumbnailUrl;

                                            }
                                            videosArray[v].thumbnailUrl = "https://resources.sonyliv.com/image/fetch/h_147,w_260,c_fill,fl_lossy,f_auto,q_auto,e_contrast:30,e_brightness:10/" + videosArray[v].assetLandscapeImage;
                                            // if (videosArray.length !== 0) {

                                            var data_title_static = '';
                                            if (videosArray[v].type) {
                                                data_title_static += videosArray[v].type;
                                            }
                                            if (videosArray[v].genre) {
                                                data_title_static += '  . ' + videosArray[v].genre;
                                            }

                                            if (videosArray[v].classification) {
                                                data_title_static += '  . ' + videosArray[v].classification;
                                            }
                                            ASSETS_ITEM_ID_WITH_SLIDER=class_name+"_"+v;

                                            var imgType = 'bgImg';
                                            if (!videosArray[v].tvBackgroundImage) {
                                                videosArray[v].tvBackgroundImage = videosArray[v].thumbnailUrl;
                                                imgType = 'thumbnailImg';
                                            }
                                            if (videosArray[v].subscriptionMode == 'Free') {
                                                ulBody += '<li id="'+ASSETS_ITEM_ID_WITH_SLIDER+'" data-cw="true" data-progress="'+Math.ceil((videosArray[v].videoStart/videosArray[v].totalLength)*100)+'" premium="0" data-pagination="false"  data-title-static="' + data_title_static + '"  poster_title="' + videosArray[v].title + '" poster_desc="' + videosArray[v].shortDesc + '"><a  onclick="homebackground(\'' + videosArray[v].id + '\', \'' + videosArray[v].vid + '\',\'' + videosArray[v].classification + '\', \'' + videosArray[v].type + '\' , \'' + videosArray[v].subscriptionMode + '\');" href="javascript:void(0);" ><img data-imgtype="' + imgType + '" data-img="' + videosArray[v].tvBackgroundImage + '" src= ' + videosArray[v].thumbnailUrl + ' /></a><div class="progressbar"><div style="width: '+Math.ceil((videosArray[v].videoStart/videosArray[v].totalLength)*100)+'%;"></div></div></li>';
                                                assetsdata.push(videosArray[v].thumbnailUrl);
                                            } else if (videosArray[v].subscriptionMode == "SVOD") {
                                                ulBody += '<li id="'+ASSETS_ITEM_ID_WITH_SLIDER+'" data-cw="true" data-progress="'+Math.ceil((videosArray[v].videoStart/videosArray[v].totalLength)*100)+'" premium="1" data-pagination="false"  data-title-static="' + data_title_static + '"  poster_title="' + videosArray[v].title + '" poster_desc="' + videosArray[v].shortDesc + '" > <span><img data-imgtype="' + imgType + '" data-img="' + videosArray[v].tvBackgroundImage + '" src="'+PREMIUM_ICONS_URL+'"></span><a onclick="homebackground(\'' + videosArray[v].id + '\', \'' + videosArray[v].vid + '\',\'' + videosArray[v].classification + '\', \'' + videosArray[v].type + '\' , \'' + videosArray[v].subscriptionMode + '\');" href="javascript:void(0);" > <img  data-imgtype="' + imgType + '" data-img="' + videosArray[v].tvBackgroundImage + '" src= ' + videosArray[v].thumbnailUrl + ' /></a><div class="progressbar"><div style="width: '+Math.ceil((videosArray[v].videoStart/videosArray[v].totalLength)*100)+'%;"></div></div></li>';
                                            } else if (videosArray[v].subscriptionMode == "TVOD") {
                                                ulBody += '<li  id="'+ASSETS_ITEM_ID_WITH_SLIDER+'" data-cw="true" data-progress="'+Math.ceil((videosArray[v].videoStart/videosArray[v].totalLength)*100)+'" premium="2" data-pagination="false"  data-title-static="' + data_title_static + '"  poster_title="' + videosArray[v].title + '" poster_desc="' + videosArray[v].shortDesc + '" ><span> <img data-imgtype="' + imgType + '" data-img="' + videosArray[v].tvBackgroundImage + '" src="../img/icon/rent.png"></span><a onclick="homebackground(\'' + videosArray[v].id + '\', \'' + videosArray[v].vid + '\',\'' + videosArray[v].classification + '\', \'' + videosArray[v].type + '\' , \'' + videosArray[v].subscriptionMode + '\');" href="javascript:void(0);" > <img data-imgtype="' + imgType + '" data-img="' + videosArray[v].tvBackgroundImage + '" src= ' + videosArray[v].thumbnailUrl + ' /></a><div class="progressbar"><div style="width: '+Math.ceil((videosArray[v].videoStart/videosArray[v].totalLength)*100)+'%;"></div></div></li>';
                                            }
                                            // }
                                        }
                                    } 
                                } */
                                ulBody += "</ul> </div>";
                                $("#slider").append(ulBody);
                            }
                        }
                    }


                } else {
                    //alert("hey");

                    var pidx = 1;
                    for (var b = 0; b < config_bands_globalx.length; b++) {
                        for (var p = 0; p < data.length; p++) {
                            let index_title = data[p].id;
                            titlenamevalue = config_bands_globalx[b].title;
                            //if (index_title == config_bands_globalx[b].id && ((titlenamevalue != 'Continue Watching' && videosArray.length) || (data[p].hasOwnProperty('assets') && data[p].assets.length))) {
                            if (index_title == config_bands_globalx[b].id && ((titlenamevalue == 'Continue Watching' && videosArray.length) || ( titlenamevalue != 'Continue Watching'  && data[p].hasOwnProperty('assets') && data[p].assets.length))) {
                                pidx++

                                titlenamev.push(config_bands_globalx[b].title);
                                $("#slider").append('<p > ' + titlenamevalue + '</p>');
                                class_name = 'slider' + pidx;
                                var ulBody = "<div id=" + class_name + " class=\"test_name\"><ul class='item-ul'>";
                                if (titlenamevalue != 'Continue Watching') 
                {
                                    for (var q = 0; q < data[p].assets.length; q++) {
                                        // if (data[p].assets.length !== 0) {
                                         if(!data[p].assets[q].assetLandscapeImage){
                                             data[p].assets[q].assetLandscapeImage = data[p].assets[q].thumbnailUrl;

                                        }
                                        data[p].assets[q].thumbnailUrl = "https://resources.sonyliv.com/image/fetch/h_147,w_260,c_fill,fl_lossy,f_auto,q_auto,e_contrast:30,e_brightness:10/" + data[p].assets[q].assetLandscapeImage ;
                                        var imgType = 'bgImg';
                                        var data_title_static = '';
                                        if (data[p].assets[q].type) {
                                            data_title_static += data[p].assets[q].type;
                                        }
                                        if (data[p].assets[q].genre) {
                                            data_title_static += '<span> . </span>' + data[p].assets[q].genre;
                                        }

                                        if (data[p].assets[q].classification) {
                                            data_title_static += '<span> . </span>' + data[p].assets[q].classification;
                                        }

                                        if (data[p].assets[q].posterUrl != "null" && data[p].assets[q].posterUrl != "undefined" && data[p].assets[q].posterUrl != ""  && data[p].assets[q].posterUrl != undefined ) {
                                            data[p].assets[q].tvBackgroundImage = data[p].assets[q].posterUrl;
                                        }else if (data[p].assets[q].assetLandscapeImage != "null" && data[p].assets[q].assetLandscapeImage != "undefined" && data[p].assets[q].assetLandscapeImage != "" ) {
                                            data[p].assets[q].tvBackgroundImage = data[p].assets[q].assetLandscapeImage;
                                        }else  {
                                            data[p].assets[q].tvBackgroundImage = data[p].assets[q].thumbnailUrl;
                                            imgType = 'thumbnailImg';
                                        }
                                        ASSETS_ITEM_ID_WITH_SLIDER=class_name+"_"+q;
                                        if (data[p].assets[q].subscriptionMode == 'Free') {
                                            ulBody += '<li id="'+ASSETS_ITEM_ID_WITH_SLIDER+'" premium="0" data-pagination="true" data-title-static="' + data_title_static + '"  data-vcount="' + (q + 1) + '" data-scount="' + pidx + '" data-band="' + index_title + '" data-banddata="' + config_bands_globalx[b].data + '" data-vtype="' + config_bands_globalx[b].type + '" poster_title="' + data[p].assets[q].title + '" poster_desc="' + data[p].assets[q].shortDesc + '"><a  onclick="homebackground(\'' + data[p].assets[q].id + '\', \'' + data[p].assets[q].vid + '\',\'' + data[p].assets[q].classification + '\', \'' + data[p].assets[q].type + '\' , \'' + data[p].assets[q].subscriptionMode + '\');"  href="javascript:void(0);" ><img data-imgtype="' + imgType + '" data-img="' + data[p].assets[q].tvBackgroundImage + '" src= ' + data[p].assets[q].thumbnailUrl + ' /></a></li>';
                                            assetsdata.push(data[p].assets[q].thumbnailUrl);
                                        } else if (data[p].assets[q].subscriptionMode == "SVOD") {
                                            var premiumImg = (localStorage.getItem('isUserPremium')!=null && (localStorage.getItem('isUserPremium')=='true')) ? '../img/icon/ic_premium_unlocked.png':'../img/icon/premium1.png';
                                            ulBody += '<li id="'+ASSETS_ITEM_ID_WITH_SLIDER+'" premium="1" data-pagination="true" data-title-static="' + data_title_static + '"  data-vcount="' + (q + 1) + '" data-scount="' + pidx + '" data-band="' + index_title + '" data-banddata="' + config_bands_globalx[b].data + '" data-vtype="' + config_bands_globalx[b].type + '" poster_title="' + data[p].assets[q].title + '" poster_desc="' + data[p].assets[q].shortDesc + '" > <span><img data-imgtype="' + imgType + '" data-img="' + data[p].assets[q].tvBackgroundImage + '" src="'+premiumImg+'"></span><a  onclick="homebackground(\'' + data[p].assets[q].id + '\', \'' + data[p].assets[q].vid + '\',\'' + data[p].assets[q].classification + '\', \'' + data[p].assets[q].type + '\' , \'' + data[p].assets[q].subscriptionMode + '\');" href="javascript:void(0);" > <img  data-imgtype="' + imgType + '" data-img="' + data[p].assets[q].tvBackgroundImage + '" src= ' + data[p].assets[q].thumbnailUrl + ' /></a></li>';
                                        } else if (data[p].assets[q].subscriptionMode == "TVOD") {
                                            var rentImg = (localStorage.getItem('RENT_SUBSCRIPTIONS')!=null && (localStorage.getItem('RENT_SUBSCRIPTIONS')).indexOf(data[p].assets[q].id)>-1) ? '../img/icon/ic_rented.png':'../img/icon/rent.png';
                                            ulBody += '<li id="'+ASSETS_ITEM_ID_WITH_SLIDER+'" premium="2" data-pagination="true" data-title-static="' + data_title_static + '"  data-vcount="' + (q + 1) + '" data-scount="' + pidx + '" data-band="' + index_title + '" data-banddata="' + config_bands_globalx[b].data + '" data-vtype="' + config_bands_globalx[b].type + '" poster_title="' + data[p].assets[q].title + '" poster_desc="' + data[p].assets[q].shortDesc + '" ><span><img data-imgtype="' + imgType + '" data-img="' + data[p].assets[q].tvBackgroundImage + '" src="'+rentImg+'"></span><a  onclick="homebackground(\'' + data[p].assets[q].id + '\', \'' + data[p].assets[q].vid + '\',\'' + data[p].assets[q].classification + '\', \'' + data[p].assets[q].type + '\' , \'' + data[p].assets[q].subscriptionMode + '\');" href="javascript:void(0);" > <img  data-imgtype="' + imgType + '" data-img="' + data[p].assets[q].tvBackgroundImage + '" src= ' + data[p].assets[q].thumbnailUrl + ' /></a></li>';
                                        }
                                        // }
                                    }
                                } 
                else 
                {
                                    if (videosArray.length) {
                                        for (var v = 0; v < videosArray.length; v++) {

                                         if(!videosArray[v].assetLandscapeImage){
                                             videosArray[v].assetLandscapeImage = videosArray[v].thumbnailUrl;

                                        }
                                            videosArray[v].thumbnailUrl = "https://resources.sonyliv.com/image/fetch/h_147,w_260,c_fill,fl_lossy,f_auto,q_auto,e_contrast:30,e_brightness:10/" + videosArray[v].assetLandscapeImage
                                            var imgType = 'bgImg';
                                            if (!videosArray[v].tvBackgroundImage) {
                                                videosArray[v].tvBackgroundImage = videosArray[v].thumbnailUrl;
                                                imgType = 'thumbnailImg';
                                            }

                                            var data_title_static = '';
                                            if (videosArray[v].type) {
                                                data_title_static += videosArray[v].type;
                                            }
                                            if (videosArray[v].genre) {
                                                data_title_static += '<span> . </span>' + videosArray[v].genre;
                                            }

                                            if (videosArray[v].classification) {
                                                data_title_static += '<span> . </span>' + videosArray[v].classification;
                                            }

                                            


                                            if (videosArray[v].posterUrl != "null" && videosArray[v].posterUrl != "undefined" && videosArray[v].posterUrl != "") {
                                                videosArray[v].tvBackgroundImage = videosArray[v].posterUrl;
                                            }else if (videosArray[v].tvBackgroundImage != "null" && videosArray[v].tvBackgroundImage != "undefined" && videosArray[v].tvBackgroundImage != "") {
                                               videosArray[v].tvBackgroundImage =videosArray[v].assetLandscapeImage;
                                            }else {
                                                videosArray[v].tvBackgroundImage = videosArray[v].thumbnailUrl;
                                                imgType = 'thumbnailImg';
                                            }



                                            ASSETS_ITEM_ID_WITH_SLIDER=class_name+"_"+v;
                                            if (videosArray[v].subscriptionMode == 'Free') {
                                                ulBody += '<li id="'+ASSETS_ITEM_ID_WITH_SLIDER+'" data-cw="true" data-progress="'+Math.ceil((videosArray[v].videoStart/videosArray[v].totalLength)*100)+'" premium="0" data-pagination="false"  data-title-static="' + data_title_static + '"  poster_title="' + videosArray[v].title + '" poster_desc="' + videosArray[v].shortDesc + '"><a  onclick="homebackground(\'' + videosArray[v].id + '\', \'' + videosArray[v].vid + '\',\'' + videosArray[v].classification + '\', \'' + videosArray[v].type + '\' , \'' + videosArray[v].subscriptionMode + '\');" href="javascript:void(0);" ><img data-imgtype="' + imgType + '" data-img="' + videosArray[v].tvBackgroundImage + '" src= ' + videosArray[v].thumbnailUrl + ' /></a><div class="progressbar1"><div style="width: '+Math.ceil((videosArray[v].videoStart/videosArray[v].totalLength)*100)+'%;"></div></div></li>';
                                                assetsdata.push(videosArray[v].thumbnailUrl);
                                            } else if (videosArray[v].subscriptionMode == "SVOD") {
                                                var premiumImg = (localStorage.getItem('isUserPremium')!=null && (localStorage.getItem('isUserPremium')=='true')) ? '../img/icon/ic_premium_unlocked.png':'../img/icon/premium1.png';
                                                ulBody += '<li id="'+ASSETS_ITEM_ID_WITH_SLIDER+'" data-cw="true" data-progress="'+Math.ceil((videosArray[v].videoStart/videosArray[v].totalLength)*100)+'" premium="1" data-pagination="false"  data-title-static="' + data_title_static + '"  poster_title="' + videosArray[v].title + '" poster_desc="' + videosArray[v].shortDesc + '" > <span><img data-imgtype="' + imgType + '" data-img="' + videosArray[v].tvBackgroundImage + '" src="'+premiumImg+'"></span><a onclick="homebackground(\'' + videosArray[v].id + '\', \'' + videosArray[v].vid + '\',\'' + videosArray[v].classification + '\', \'' + videosArray[v].type + '\' , \'' + videosArray[v].subscriptionMode + '\');" href="javascript:void(0);" > <img  data-imgtype="' + imgType + '" data-img="' + videosArray[v].tvBackgroundImage + '" src= ' + videosArray[v].thumbnailUrl + ' /></a><div class="progressba1r"><div style="width: '+Math.ceil((videosArray[v].videoStart/videosArray[v].totalLength)*100)+'%;"></div></div></li>';
                                            } else if (videosArray[v].subscriptionMode == "TVOD") {
                                                var rentImg = (localStorage.getItem('RENT_SUBSCRIPTIONS')!=null && (localStorage.getItem('RENT_SUBSCRIPTIONS')).indexOf(data[p].assets[q].id)>-1) ? '../img/icon/ic_rented.png':'../img/icon/rent.png';
                                                ulBody += '<li  id="'+ASSETS_ITEM_ID_WITH_SLIDER+'" data-cw="true" data-progress="'+Math.ceil((videosArray[v].videoStart/videosArray[v].totalLength)*100)+'" premium="2" data-pagination="false"  data-title-static="' + data_title_static + '"  poster_title="' + videosArray[v].title + '" poster_desc="' + videosArray[v].shortDesc + '" ><span><span><img data-imgtype="' + imgType + '" data-img="' + data[p].assets[q].tvBackgroundImage + '" src="'+rentImg+'"></span><a onclick="homebackground(\'' + videosArray[v].id + '\', \'' + videosArray[v].vid + '\',\'' + videosArray[v].classification + '\', \'' + videosArray[v].type + '\' , \'' + videosArray[v].subscriptionMode + '\');" href="javascript:void(0);" > <img data-imgtype="' + imgType + '" data-img="' + videosArray[v].tvBackgroundImage + '" src= ' + videosArray[v].thumbnailUrl + ' /></a>TVOD<div class="progressbar1"><div style="width: '+Math.ceil((videosArray[v].videoStart/videosArray[v].totalLength)*100)+'%;"></div></div></li>';
                                            }
                                            // }
                                        }
                                    }
                                }
                                ulBody += "</ul> </div>";
                                $("#slider").append(ulBody);
                            }
                        }
                    }

                }

                var CURRENT_SLIDER=localStorage.getItem("CURRENT_SLIDER");
                var CURRENT_SLIDER_ELEMENT=localStorage.getItem("CURRENT_SLIDER_ELEMENT");
                //alert($('#'+CURRENT_SLIDER+'_'+CURRENT_SLIDER_ELEMENT).length);
                //if(($('#'+CURRENT_SLIDER).length) && ($('#'+CURRENT_SLIDER+'_'+CURRENT_SLIDER_ELEMENT).length) && parseInt(CURRENT_SLIDER_ELEMENT) < 9){
                  if(CURRENT_SLIDER!=null && CURRENT_SLIDER!=undefined){
                    // if(CURRENT_SLIDER_ELEMENT<=3){
                    //     $('#'+CURRENT_SLIDER+' #'+CURRENT_SLIDER+"_"+CURRENT_SLIDER_ELEMENT+" a").focus();
                    // }else{
                    //     $('#'+CURRENT_SLIDER+' #'+CURRENT_SLIDER+"_1 a").focus();
                    // }

                    //alert('#'+CURRENT_SLIDER+"_"+CURRENT_SLIDER_ELEMENT+" a")
                    // document.getElementById('#'+CURRENT_SLIDER+"_"+CURRENT_SLIDER_ELEMENT+" a").scrollIntoView();
                    // $('#'+CURRENT_SLIDER+"_"+CURRENT_SLIDER_ELEMENT+" a").focus();
                    FIRST_TIME_CALL_BOTTOM=CURRENT_SLIDER.replace ( /[^\d.]/g, '' );

                    $("#menuTitle").html('');
                    $('#home-page-title').html($('a:focus').parent().attr('poster_title'));
                    $('#home-page-state').html($('a:focus').parent().data('title-static'));
                    $('#home-page-description').html($('a:focus').parent().attr('poster_desc'));
                    if ($('a:focus').parent().attr('premium') == 1) {
                        $('.rent img').attr('src', '../img/icon/premium1.png');
                        $('.rent').css('visibility', 'visible');
                    } else if ($('a:focus').parent().attr('premium') == 2) {
                       
                        $('.rent img').attr('src', '../img/icon/rent.png');
                        $('.rent').css('visibility', 'visible');
                    } else {
                        $('.rent').css('visibility', 'hidden');
                    }

                    var finalImage = $('a:focus img').data('img');
                    if ($('a:focus img').data('imgtype') == 'bgImg') {
                        $('#home-page-poster').css('background-image','url("../img/gradient-copy.png"), url("' + finalImage + '") ');
                        $('#home-page-poster').css('background-size', '80%, 80%');

                    } else if ($('a:focus  img').data('imgtype') == 'thumbnailImg') {
                        $('#home-page-poster').css('background-image', 'url("../img/gradient-copy.png"),url("' + finalImage + '")');
                        $('#home-page-poster').css('background-size', '80% 80%, 70% 80%');
                        $('#home-page-poster').css('background-position', 'right top, right top');

                    } else {
                        $('#home-page-poster').css('background-image', 'url("../img/Home_menu_bg.png"), url("../img/gradient-copy.png")');
                        $('#home-page-poster').css('background-size', '80%, 80%');

                    }
                    if($('a:focus').parent().data('cw')){
                        $('#progressDisplay').html('<div class="progressbar"><div style="width: '+$('a:focus').parent().data('progress')+'%;"></div></div>');
                    } else{
                        $('#progressDisplay').html('');
                    }

                 } 
                //}else{
                    //alert($('#'+CURRENT_SLIDER).length);
                    //alert('#'+CURRENT_SLIDER+"_"+CURRENT_SLIDER_ELEMENT);
                //}
            }

            if(window.location.href.indexOf("HomeMenu.html") > -1) {
                //alert('test')
                setTimeout(function(){ 
                    $('#'+CURRENT_SLIDER+"_"+CURRENT_SLIDER_ELEMENT+" a").focus();
                    localStorage.removeItem("CURRENT_SLIDER");
                    localStorage.removeItem("CURRENT_SLIDER_ELEMENT");
                    localStorage.removeItem("CURRENT_SLIDER_ELEMENT_BAND");
                }, 1000);
            }
        }
    });
}

function homebackground(id, vid, classification, type, subscriptionMode) {
    //     alert("id" +id);
       //alert("subscriptionMode"+subscriptionMode);
        //alert("classification"+classification);
        //alert("type"+type);
    //     alert("vid"+vid);
    //     alert("if(activeSubscriptions_date){"+activeSubscriptions_date);

    // alert("Classification na Type are "+classification+"and"+type); 
    localStorage.removeItem("return_back");
    localStorage.setItem("return_back", "HomeMenu.html");
    localStorage.setItem("activeSubscriptions_date", activeSubscriptions_date);
    var current_focus=$('a:focus').parent().attr('id');
    var band = $('a:focus').parent().data('band');
    if(current_focus != "undefined"){
        current_focus=current_focus.split("_");
        if(current_focus.length === 2){
            localStorage.setItem("CURRENT_SLIDER", current_focus[0]);
            localStorage.setItem("CURRENT_SLIDER_ELEMENT", current_focus[1]);
            localStorage.setItem("CURRENT_SLIDER_ELEMENT_BAND", band);
        }
    }
   
    if (classification == "Sport") {

        if (type == "Specials" || type == "Clips" || type == "Highlights" || type == "Studio" || type == "Episodes" || type == "Webisode" || type == "Full Matches" || type == "Interview") {

            window.location.href="player.html?videoid=" + vid+ "&vid=" +id;
        } else if (type == "Sport") {
            window.location.href="sport.html?videoid=" + id;
        } else if (type == "LIVE") {
            if (subscriptionMode == "SVOD" || subscriptionMode ==  "TVOD") {
                window.location.href="permiummembers.html";
            } else {
                window.location.href="live.html?videoid=" + vid;

            }
        }else{
            window.location.href="player.html?videoid=" + vid+ "&vid=" +id;
        }


    }
     else if (classification == "SHow") {
       // if(menubar_event =="show"){
            // alert("show");
            //
         
       if (subscriptionMode == "SVOD" || subscriptionMode == "TVOD") {
            console.log("notworking" + subscriptionMode);
            localStorage.setItem('subscriptionMode', subscriptionMode);
        }

    if (type == "Episodes") {

            window.location.href="player.html?videoid=" + vid+ "&vid=" +id ;

        } else if (type == "Music") {

            window.location.href="player.html?videoid=" + vid+ "&vid=" +id ;

        } else if (type == "Promos") {

            window.location.href="player.html?videoid=" + vid+ "&vid=" +id ;

        } else {

       //alert("1 In the Show detailed");
            window.location.href="detail.html?videoid=" + id+"&subsmode="+subscriptionMode+"&none=type:show&none=type:promos";
        }
      }

      // else{
      //   alert("home");
      //   if (type == "Episodes") {

      //       window.location.href="player.html?videoid=" + vid;

      //   } 
      // else if (type == "Music") {

      //       window.location.href="player.html?videoid=" + vid;

      //   } else if (type == "Promos") {

      //       window.location.href="player.html?videoid=" + vid;

      //   } 
      //   else if (type == "Show") {

      //       window.location.href="player.html?videoid=" + vid;

      //   }else {

      //       window.location.href="detail.html?videoid=" + id;
      //   }
      // } 
 // }




      else if (classification == "Music") {

        if (type == "Music") {
            window.location.href="player.html?videoid=" + vid+ "&vid=" +id ;

        }



    } else if (classification == "Movie") {
        // alert("1");
        if (subscriptionMode == "SVOD" || subscriptionMode == "TVOD") {
            console.log("notworking" + subscriptionMode);
            localStorage.setItem('subscriptionMode', subscriptionMode);

            window.location.href="movie-details.html?videoid=" + id;



        } else {
            if (type == "Short film") {
                //  alert("shor films");
                window.location.href="player.html?videoid=" + vid+ "&vid=" +id ;

            } else if (type == "Full Movie") {
               window.location.href="movie-details.html?videoid=" + id;
            }

        }

    } else if (classification == "Channel") {

        localStorage.removeItem('premium_redirect');
        localStorage.removeItem('premium_redirect_param');
        localStorage.setItem('premium_redirect', "permiummembers.html");
        localStorage.setItem('premium_redirect_param', "live.html?videoid=" + vid+"&id="+id);
     if (subscriptionMode == "SVOD" || subscriptionMode == "TVOD") {
            console.log("notworking" + subscriptionMode);
            localStorage.setItem('subscriptionMode', subscriptionMode);
    }

         if(activeSubscriptions_date != undefined){
            window.location.href="live.html?videoid=" + vid+"&id="+id;

         }
        else{
            if (subscriptionMode == "SVOD" || subscriptionMode == "TVOD") {
                // alert("sovd");
            window.location.href="channel-member.html";
        } else {
            window.location.href="live.html?videoid=" + vid+"&id="+id;

        }
        }
        

    }


}

// function call for DMA Data
function getDMADetails() {
    $.ajax({
        url: 'https://api.sonyliv.com/api/auth/getDMADetails',
        headers: {
            // 'Content-Type': 'application/x-www-form-urlencoded',
            // 'x-via-device': 'true',

        },
        method: 'GET',
        dataType: 'json',
        //data: accesdoData,
        error: function (jqXHR, status, error) {

            if (jqXHR.status == 400) {
                // alert("Sorry, Please check your input information ");
            }
            if (jqXHR.status == 500) {
                //  alert("Internal server error ");
            } else {
                //  alert("An error occurred: " + status + "nError: " + error);
            }
        },
        success: function (data, status) {
            if (status == "success") {
                var DMASign = data.signature;
                var DMAchannelPartnerID = data.channelPartnerID;
            }
        }
    });
}

var assetstringArr = [];
var activeSubscriptions_date;
var newdata;
// function call for  asset_band
function asset_band_details() {
    $.ajax({
        url: 'https://api.sonyliv.com/api/configuration/asset_band_details',
        headers: {
            // 'Content-Type': 'application/x-www-form-urlencoded',
            // 'x-via-device': 'true',
        },
        method: 'GET',
        dataType: 'json',
        //data: accesdoData,
        error: function (jqXHR, status, error) {
            if (jqXHR.status == 400) {
                //   alert("Sorry, Please check your input information ");
            }
            if (jqXHR.status == 500) {
                // alert("Internal server error ");
            } else {
                //alert("An error occurred: " + status + "nError: " + error);
            }
        },
        success: function (data, status) {
            if (status == "success") {
                console.log(data);
            }

        }
    });

}

function getNextPageRecord(pageToBeFetch, sCount, bandData, band, vidType) {
    var showNextPageApi = {
        "detailsType": "basic",
        "deviceDetails": {
            "deviceId": "71066286297",
            "mfg": "GoogleChrome",
            "model": "GoogleChrome",
            "os": "Android",
            "osVer": "XXX"
        },
        "isSearchable": true,
        "searchSet": [
            {
                "action": "",
                "data": bandData,
                "id": band,
                "pageNumber": pageToBeFetch,
                "pageSize": "10",
                "sortOrder": undefined,
                "type": vidType
            }
        ]
    }

    $.ajax({
        url: 'https://api.sonyliv.com/api/v4/vod/search',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'x-via-device': 'true',
            // 'Access-Control-Allow-Origin': 'https://www.sonyliv.com/',
            // "Access-Control-Allow-Origin" : "*",
        },
        method: 'POST',
        dataType: 'json',
        data: showNextPageApi,
        error: function (jqXHR, status, error) {
            if (jqXHR.status == 400) {
                // alert("Sorry, Please check your input information ");
            }
            if (jqXHR.status == 500) {
                //  alert("Internal server error ");
            } else {
                //   alert("An error occurred: " + status + "nError: " + error);
            }
        },
        success: function (data, status) {
            if (status == "success") {
                console.log(data);
                var ulBody = "";
                $('.test_name#slider' + sCount).children('ul').children('li').attr('data-pagination', 'false');
                for (var q = 0; q < data[0].assets.length; q++) {

                    if(!data[0].assets[q].assetLandscapeImage){
                        data[0].assets[q].assetLandscapeImage = data[0].assets[q].thumbnailUrl;

                   }
                    data[0].assets[q].thumbnailUrl = "https://resources.sonyliv.com/image/fetch/h_147,w_260,c_fill,fl_lossy,f_auto,q_auto,e_contrast:30,e_brightness:10/" + data[0].assets[q].assetLandscapeImage;
                    if (data[0].assets.length !== 0) {
                        var imgType = 'bgImg';

                        // if (!data[0].assets[q].tvBackgroundImage) {
                        //     data[0].assets[q].tvBackgroundImage = data[0].assets[q].thumbnailUrl;
                        //     imgType = 'thumbnailImg';
                        // }


                        if (data[0].assets[q].posterUrl != "null" && data[0].assets[q].posterUrl != "undefined" && data[0].assets[q].posterUrl != "" ) {
                           data[0].assets[q].tvBackgroundImage = data[0].assets[q].posterUrl;
                        }else if (data[0].assets[q].assetLandscapeImage != "null" && data[0].assets[q].assetLandscapeImage != "undefined" && data[0].assets[q].assetLandscapeImage != "" ) {
                            data[0].assets[q].tvBackgroundImage = data[0].assets[q].assetLandscapeImage;
                        }else {
                            data[0].assets[q].tvBackgroundImage = data[0].assets[q].thumbnailUrl;
                            imgType = 'thumbnailImg';
                        }

                        var data_title_static = '';
                        if (data[0].assets[q].type) {
                            data_title_static += data[0].assets[q].type;
                        }
                        if (data[0].assets[q].genre) {
                            data_title_static += '<span> . <span>' + data[0].assets[q].genre;
                        }

                        if (data[0].assets[q].classification) {
                            data_title_static += '<span> . <span>' + data[0].assets[q].classification;
                        }
                        ASSETS_ITEM_ID_WITH_SLIDER='slider' + sCount +"_"+((pageToBeFetch * 10) + q );
                        if (data[0].assets[q].subscriptionMode == 'Free') {
                            ulBody += '<li id="'+ASSETS_ITEM_ID_WITH_SLIDER+'"  premium="0" data-pagination="true" data-title-static="' + data_title_static + '"  data-vcount="' + ((pageToBeFetch * 10) + q + 1) + '" data-scount="' + sCount + '" data-band="' + band + '" data-banddata="' + bandData + '"  data-vtype="' + vidType + '" poster_title="' + data[0].assets[q].title + '" poster_desc="' + data[0].assets[q].shortDesc + '"><a  onclick="homebackground(\'' + data[0].assets[q].id + '\', \'' + data[0].assets[q].vid + '\',\'' + data[0].assets[q].classification + '\', \'' + data[0].assets[q].type + '\' , \'' + data[0].assets[q].subscriptionMode + '\');"  href="javascript:void(0);" ><img data-imgtype="' + imgType + '" data-img="' + data[0].assets[q].tvBackgroundImage + '" src= ' + data[0].assets[q].thumbnailUrl + ' /></a></li>';
                            assetsdata.push(data[0].assets[q].thumbnailUrl);
                        } else if (data[0].assets[q].subscriptionMode == "SVOD") {
                            ulBody += '<li id="'+ASSETS_ITEM_ID_WITH_SLIDER+'"  premium="1" data-pagination="true" data-title-static="' + data_title_static + '"  data-vcount="' + ((pageToBeFetch * 10) + q + 1) + '" data-scount="' + sCount + '" data-band="' + band + '" data-banddata="' + bandData + '" data-vtype="' + vidType + '" poster_title="' + data[0].assets[q].title + '" poster_desc="' + data[0].assets[q].shortDesc + '" > <span><img data-imgtype="' + imgType + '" data-img="' + data[0].assets[q].tvBackgroundImage + '" src="'+PREMIUM_ICONS_URL+'"></span><a  onclick="homebackground(\'' + data[0].assets[q].id + '\', \'' + data[0].assets[q].vid + '\',\'' + data[0].assets[q].classification + '\', \'' + data[0].assets[q].type + '\' , \'' + data[0].assets[q].subscriptionMode + '\');" href="javascript:void(0);" > <img  data-imgtype="' + imgType + '" data-img="' + data[0].assets[q].tvBackgroundImage + '" src= ' + data[0].assets[q].thumbnailUrl + ' /></a></li>';
                        } else if (data[0].assets[q].subscriptionMode == "TVOD") {
                            ulBody += '<li id="'+ASSETS_ITEM_ID_WITH_SLIDER+'" premium="0" data-pagination="true" data-title-static="' + data_title_static + '"  data-vcount="' + ((pageToBeFetch * 10) + q + 1) + '" data-scount="' + sCount + '" data-band="' + band + '" data-banddata="' + bandData + '" data-vtype="' + vidType + '" poster_title="' + data[0].assets[q].title + '" poster_desc="' + data[0].assets[q].shortDesc + '" ><span ><a  onclick="homebackground(\'' + data[0].assets[q].id + '\', \'' + data[0].assets[q].vid + '\',\'' + data[0].assets[q].classification + '\', \'' + data[0].assets[q].type + '\' , \'' + data[0].assets[q].subscriptionMode + '\');" href="javascript:void(0);" > <img  data-imgtype="' + imgType + '" data-img="' + data[0].assets[q].tvBackgroundImage + '" src= ' + data[0].assets[q].thumbnailUrl + ' /></a></li>';
                        }

                        // if (data[0].assets[q].subscriptionMode == 'Free') {
                        //     ulBody += '<li premium="0" data-pagination="true" data-title-static="' + data_title_static + '"  data-vcount="' + ((pageToBeFetch * 10) + q + 1) + '" data-scount="' + sCount + '" data-band="' + band + '" data-banddata="' + bandData + '"  data-vtype="' + vidType + '" poster_title="' + data[0].assets[q].title + '" poster_desc="' + data[0].assets[q].shortDesc + '"><a  onclick="homebackground(\'' + data[0].assets[q].id + '\', \'' + data[0].assets[q].vid + '\',\'' + data[0].assets[q].classification + '\', \'' + data[0].assets[q].type + '\' , \'' + data[0].assets[q].subscriptionMode + '\');"  href="javascript:void(0);" ><img data-imgtype="' + imgType + '" data-img="' + data[0].assets[q].tvBackgroundImage + '" src= ' + data[0].assets[q].thumbnailUrl + ' /></a></li>';
                        //     assetsdata.push(data[0].assets[q].thumbnailUrl);
                        // } else if (data[0].assets[q].subscriptionMode == "SVOD") {
                        //     ulBody += '<li premium="1" data-pagination="true" data-title-static="' + data_title_static + '"  data-vcount="' + ((pageToBeFetch * 10) + q + 1) + '" data-scount="' + sCount + '" data-band="' + band + '" data-banddata="' + bandData + '" data-vtype="' + vidType + '" poster_title="' + data[0].assets[q].title + '" poster_desc="' + data[0].assets[q].shortDesc + '" > <span><img data-imgtype="' + imgType + '" data-img="' + data[0].assets[q].tvBackgroundImage + '" src="'+PREMIUM_ICONS_URL+'"></span><a  onclick="homebackground(\'' + data[0].assets[q].id + '\', \'' + data[0].assets[q].vid + '\',\'' + data[0].assets[q].classification + '\', \'' + data[0].assets[q].type + '\' , \'' + data[0].assets[q].subscriptionMode + '\')" href="javascript:void(0);" > <img  data-imgtype="' + imgType + '" data-img="' + data[0].assets[q].tvBackgroundImage + '" src= ' + data[0].assets[q].thumbnailUrl + ' /></a></li>';
                        // } else if (data[0].assets[q].subscriptionMode == "TVOD") {
                        //     ulBody += '<li  premium="0" data-pagination="true" data-title-static="' + data_title_static + '"  data-vcount="' + ((pageToBeFetch * 10) + q + 1) + '" data-scount="' + sCount + '" data-band="' + band + '" data-banddata="' + bandData + '" data-vtype="' + vidType + '" poster_title="' + data[0].assets[q].title + '" poster_desc="' + data[0].assets[q].shortDesc + '" ><span ><a onclick="homebackground(\'' + data[0].assets[q].id + '\', \'' + data[0].assets[q].vid + '\',\'' + data[0].assets[q].classification + '\', \'' + data[0].assets[q].type + '\' , \'' + data[0].assets[q].subscriptionMode + '\')" href="javascript:void(0);" > <img data-imgtype="' + imgType + '" data-img="' + data[0].assets[q].tvBackgroundImage + '" src= ' + data[0].assets[q].thumbnailUrl + ' /></a>TVOD</li>';
                        // }
                    }
                }
                $('#slider' + sCount + ' ul').append(ulBody);
            }
        }
    });
}

var myVar = setInterval(session, 1800000);

function search_page() {
    // alert("hit");
    window.location.href="template/search.html";
}
