(function () {
    'use strict';
    
    var module = {
        options: [],
        header: [navigator.platform, navigator.userAgent, navigator.appVersion, navigator.vendor, window.opera],
        dataos: [
            { name: 'Windows Phone', value: 'Windows Phone', version: 'OS' },
            { name: 'Windows', value: 'Win', version: 'NT' },
            { name: 'iPhone', value: 'iPhone', version: 'OS' },
            { name: 'iPad', value: 'iPad', version: 'OS' },
            { name: 'Kindle', value: 'Silk', version: 'Silk' },
            { name: 'Android', value: 'Android', version: 'Android' },
            { name: 'PlayBook', value: 'PlayBook', version: 'OS' },
            { name: 'BlackBerry', value: 'BlackBerry', version: '/' },
            { name: 'Macintosh', value: 'Mac', version: 'OS X' },
            { name: 'Linux', value: 'Linux', version: 'rv' },
            { name: 'Palm', value: 'Palm', version: 'PalmOS' }
        ],
        databrowser: [
            { name: 'Chrome', value: 'Chrome', version: 'Chrome' },
            { name: 'Firefox', value: 'Firefox', version: 'Firefox' },
            { name: 'Safari', value: 'Safari', version: 'Version' },
            { name: 'Internet Explorer', value: 'MSIE', version: 'MSIE' },
            { name: 'Opera', value: 'Opera', version: 'Opera' },
            { name: 'BlackBerry', value: 'CLDC', version: 'CLDC' },
            { name: 'Mozilla', value: 'Mozilla', version: 'Mozilla' }
        ],
        init: function () {
            var agent = this.header.join(' '),
                os = this.matchItem(agent, this.dataos),
                browser = this.matchItem(agent, this.databrowser);
            
            return { os: os, browser: browser };
        },
        matchItem: function (string, data) {
            var i = 0,
                j = 0,
                html = '',
                regex,
                regexv,
                match,
                matches,
                version;
            
            for (i = 0; i < data.length; i += 1) {
                regex = new RegExp(data[i].value, 'i');
                match = regex.test(string);
                if (match) {
                    regexv = new RegExp(data[i].version + '[- /:;]([\\d._]+)', 'i');
                    matches = string.match(regexv);
                    version = '';
                    if (matches) { if (matches[1]) { matches = matches[1]; } }
                    if (matches) {
                        matches = matches.split(/[._]+/);
                        for (j = 0; j < matches.length; j += 1) {
                            if (j === 0) {
                                version += matches[j] + '.';
                            } else {
                                version += matches[j];
                            }
                        }
                    } else {
                        version = '0';
                    }
                    return {
                        name: data[i].name,
                        version: parseFloat(version)
                    };
                }
            }
            return { name: 'unknown', version: 0 };
        }
    };
    var e = module.init(),
        debug = '';
         //alert(e.os.name);
         //alert(e.browser.name);
         
    localStorage.setItem("broserName", e.browser.name);
    debug += 'os.name = ' + e.os.name + '<br/>';
    debug += 'os.version = ' + e.os.version + '<br/>';
    debug += 'browser.name = ' + e.browser.name + '<br/>';
    debug += 'browser.version = ' + e.browser.version + '<br/>';
    
    debug += '<br/>';
    debug += 'navigator.userAgent = ' + navigator.userAgent + '<br/>';
    debug += 'navigator.appVersion = ' + navigator.appVersion + '<br/>';
    debug += 'navigator.platform = ' + navigator.platform + '<br/>';
    debug += 'navigator.vendor = ' + navigator.vendor + '<br/>';
   
}());

//--Get Premium user details
function checkPremiumUser() {
    var access_token = localStorage.getItem("access_token");
    if (access_token!=undefined && access_token!=null && access_token!="") {
        var getAllSubscription_data = {
            "channelPartnerID":"MSMIND",
        }
        var Subscription_data  = JSON.stringify(getAllSubscription_data);
        $.ajax({
            url:'https://api.sonyliv.com/api/subscription/getAllSubscriptions',
            headers: {
                'Content-Type':'application/json',
                'x-via-device':'true',
                'Authorization' :access_token,
            },
            method: 'POST',
            dataType: 'json',
            data: Subscription_data,
            error: function (jqXHR, status, error) {
                if (jqXHR.status == 400) {
                    console.log("Access token is not correct one in device.js.!");
                }
                else {
                    console.log("Error occurred in Premium user details device.js");
                }
                alert('errosr '+ jqXHR.status)
            },
            success: function(data ,status ){
                alert('sccess')
                if(status == "success"){
                    if(data.activeSubscriptions[0]) {
                        var duration = parseInt(data.activeSubscriptions[0].validityTill);
                        // var activeSubscriptions_date = new Date(duration);
                        // if(activeSubscriptions_date) {
                        if(duration) {
                            // var today = new Date();
                            // if(today.getTime() < activeSubscriptions_date.getTime())
                            // {
                            //     return true;
                            // }
                            return true;
                        }
                    }
                }
            }
        });
        alert('done')
    } else {
        return false;
    }
}

//--Get Premium user details
function getAllUserSubscriptions() {
    var accessTok = localStorage.getItem("access_token");
    var premiumRedirect = localStorage.getItem('premium_redirect');
    if(premiumRedirect!=undefined && premiumRedirect!="" && premiumRedirect!=null) {
        premiumRedirect = premiumRedirect;
    } else {
        premiumRedirect = "myaccount.html";
    }
    //debugger;
    if(accessTok) {
        localStorage.setItem("activeSubscriptions_date", undefined);
        localStorage.setItem("isUserPremium", false);
        var getAllSubscriptionData = {
            "channelPartnerID": "MSMIND1",
        }
        var SubscriptionData = JSON.stringify(getAllSubscriptionData);
        /* start ajax submission process */
        $.ajax({
            url: 'https://api.sonyliv.com/api/subscription/getAllSubscriptions',
            headers: {
                'Content-Type': 'application/json',
                'x-via-device': 'true',
                'Authorization': accessTok,
            },
            method: 'POST',
            dataType: 'json',
            data: SubscriptionData,
            error: function (jqXHR, status, error) {
                if (jqXHR.status == 400) {
                    console.log("getAllSubscriptions() - Sorry, Please check your input information ");
                } else {
                    console.log("getAllSubscriptions() - An error occurred: " + status + "nError: " + error);
                }
            },
            success: function (data, status) {
                if (status == "success") {
                    //debugger;
                    var RENT_SUBSCRIPTIONS = [];
                    var premiumRedirectParam = localStorage.getItem('premium_redirect_param');
                    for(var active_Subscriptions = 0; active_Subscriptions<data.activeSubscriptions.length; active_Subscriptions++)
                    {
                        if(data.activeSubscriptions[active_Subscriptions].subscriptionType=='SVOD') 
                        {
                            var duration = parseInt(data.activeSubscriptions[active_Subscriptions].validityTill);
                            var activeSubscriptions_date = new Date(duration);
                            localStorage.setItem("activeSubscriptions_date", activeSubscriptions_date);
                            localStorage.setItem("isUserPremium", true);

                            var isUserPremium = localStorage.getItem('isUserPremium');
                            if(premiumRedirect!=undefined && premiumRedirect!="" && premiumRedirect!=null && premiumRedirect!='myaccount.html') 
                            {
                                if(isUserPremium && (localStorage.getItem('subscriptionMode') != undefined && localStorage.getItem('subscriptionMode')=='SVOD')) 
                                {
                                    localStorage.setItem("return_back", premiumRedirect);
                                    premiumRedirect=premiumRedirectParam;
                                    break;
                                } 
                            } 
                            else 
                            {
                                premiumRedirect = "myaccount.html";
                            }
                        }

                        if(data.activeSubscriptions[active_Subscriptions].subscriptionType=='TVOD') 
                        {
                            premiumRedirect = localStorage.getItem('premium_redirect');
                            if(data.activeSubscriptions[active_Subscriptions].assets)
                            {
                                RENT_SUBSCRIPTIONS.push(data.activeSubscriptions[active_Subscriptions].assets[0].assetId); 
                            }
                            if(localStorage.getItem('subscriptionMode')!=undefined && localStorage.getItem('subscriptionMode')=='TVOD' && data.activeSubscriptions[active_Subscriptions].assets && RENT_SUBSCRIPTIONS.indexOf(localStorage.getItem('premium_redirect_param'))>-1)
                            {
                                localStorage.setItem("return_back", premiumRedirect);
                                premiumRedirect=premiumRedirectParam;
                                break;
                            }
                        }
                    }
                    localStorage.setItem('RENT_SUBSCRIPTIONS', JSON.stringify(RENT_SUBSCRIPTIONS));
                    window.location.href = (premiumRedirect==null||premiumRedirect==undefined||premiumRedirect=='null'||premiumRedirect=='undefined') ? "myaccount.html":premiumRedirect;
                    return false;
                }
            }
        });
    }
}


function sendXdrRequest() {
  var videosArrayTmp = localStorage.getItem('videos');

    var videosArray = JSON.parse(videosArrayTmp);
    var access_token = localStorage.getItem("access_token");
    var request_json= new Array();

   if (videosArray != null && videosArray.length) {
     console.log("Length is " + videosArray.length);
        for (var v = 0; v < videosArray.length; v++) {
          var video_position=videosArray[v].videoStart;
    var current_position=Math.round(video_position*1000);
	var vid;

	console.log("## videosArray[v].id is "+videosArray[v].id);
	console.log("## videosArray[v].id is "+videosArray[v].vid);
          var videoContent ={"assetId":videosArray[v].id,"offset":{"assetDuration":videosArray[v].totalLength,"position":current_position },"updated_at":+ new Date()} ;
          request_json.push(videoContent);
     //var request_json=[{"assetGenre":"testContinueWatching","assetId":videosArray[v].id,"assetShow":"testContinueWatchingShow","assetTitle":videosArray[v].title,"deviceId":"4323erwq1234qwer","offset":{"assetDuration":videosArray[v].videoStart,"position":videosArray[v].totalLength},"updated_at":+ new Date()}] 
    //var request_json=[{"assetId":videosArray[v].id,"offset":{"assetDuration":videosArray[v].totalLength,"position":videosArray[v].videoStart},"updated_at":+ new Date()}] 
        }
        $.ajax({
            url: 'https://api.sonyliv.com/api/v1/xdr',
            headers: {
             'Content-Type': 'application/json',
                'x-via-device': 'true',
                'Authorization': access_token
            },

            method: 'POST',
            dataType: 'json',
            data: JSON.stringify(request_json),
            error: function (jqXHR, status, error) {
                if (jqXHR.status == 400) {
                    alert("Sorry, ssssss Please check your input information ");


                } else {
                    alert("An error xdr occurred: " + status + "Error details: " + error);
                }
            },
            success: function (data, status) {

                console.log("XDR inseryon succesful ");
		 localStorage.removeItem("videos");
            }


   
        });
    }

}



function sendXdrData(request_json){
    var access_token = localStorage.getItem("access_token");
    if (access_token) {

        $.ajax({
            // url: 'https://api.sonyliv.com/api/v1/xdr',
            url: 'https://www.sonyliv.com/api/v1/xdr',
            headers: {
                'Content-Type': 'application/json',
                'x-via-device': 'true',
                'Authorization': access_token,
            },
            method: 'POST',
            dataType: 'json',
            data: JSON.stringify(request_json),
            error: function (jqXHR, status, error) {
                if (jqXHR.status == 400) {
                    alert("Sorry, ssssss Please check your input information ");

                } else {
                    alert("An error in cw occurred: " + status + "nError: " + error);
                }
            },
            success: function (data, status) {

		localStorage.removeItem("videos");

            }


        });
    }
}


// alert(window.location.href)
// alert(window.location.href.indexOf("player.html"))
$(document).bind("keydown keypress", function(e) {
    if (window.location.href.indexOf("HomeMenu.html") == -1 && e.which == 8 && localStorage.getItem("return_back_lock")==undefined) { // not home page & back button 
        var return_back = localStorage.getItem("return_back");
        if (return_back != undefined && return_back != null && return_back != '') {
            window.location.href = return_back;
            return false;
        }
    } else if(localStorage.getItem("return_back_lock") && e.which == 8) {
        localStorage.removeItem("return_back_lock");
        document.getElementById("overlay").style.display = "none";
        return false;
    }
});

// document.addEventListener("DOMContentLoaded", function() {
//   var lazyImages = [].slice.call(document.querySelectorAll("img"));

//   if ("IntersectionObserver" in window) {
//     let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
//       entries.forEach(function(entry) {
//         if (entry.isIntersecting) {
//          console.log("====call Images===");
//           let lazyImage = entry.target;
//           lazyImage.src = lazyImage.dataset.src;
//           lazyImage.srcset = lazyImage.dataset.srcset;
//           lazyImage.classList.remove("lazy");
//           lazyImageObserver.unobserve(lazyImage);
//         }
//       });
//     });

//     lazyImages.forEach(function(lazyImage) {
//       lazyImageObserver.observe(lazyImage);
//     });
//   } else {
//     // Possibly fall back to a more compatible method here
//   }
// });

